<?php
class User {
	private $root;
	
	public function __construct($root){
		$this->root = $root;
	}
	
	public static function isExists($name){		
		$result = dibi::query('SELECT * FROM :prefix:users WHERE jmeno = %s', $name, 'or id = %i', $name);
		if(count($result) == 0)
			return false;
		else
			return true;
	}
	
	public static function create($username, $pass, $email){		
		$error = array();
		$pass = sha1($pass); //hash
		$ip = Utilities::ip();
		
		$u=User::current();
		if($u!=false)
			$error[] = "Jsi přihlášen jako ".$u["nick"];
		if($ip == "")
			$error[] = "Nepodařilo se získat adresu IP";
		if(User::isExists($username))
			$error[] = "Uživatelský účet s tímto jménem již existuje";
		if(!Utilities::isEmail($email))
			$error[] = "Zadaná emailová adresa není validní";
		
		if(count($error) == 0){
			$data = array(
						"jmeno" 	=> $username,
						"nick" 		=> $username,
						"heslo" 	=> $pass,
						"email" 	=> $email,
						"ip" 		=> $ip,
						"kdy" 		=> time(),
						"prava" 	=> Database::getConfig("default-permision"),
						"avatar"	=> Database::getConfig("default-avatar"),
						"blokovan" 	=> 0
					);
			$result = dibi::query('INSERT INTO :prefix:users', $data);
			if(!$result)
				$error[] = "Při vytváření uživatelského účtu došlo k chybě";
		}
		
		if(count($error) == 0) 
			return true; 
		else 
			return $error;
	}
	
	public static function login($username, $pass, $save = false){
		$error = array();
		$pass = sha1($pass); //hash
		$ip = Utilities::ip();
		
		if($ip == "")
			$error[] = "Nepodařilo se získat adresu IP";
		if(!User::isExists($username))
			$error[] = "Uživatelský účet s tímto jménem neexistuje";
		
		if(count($error) == 0){
			$result = dibi::query('SELECT * FROM :prefix:users WHERE jmeno = %s', $username, 'and heslo = %s', $pass);
			if(count($result) != 0){
				$result = $result->fetch();
				
				if($save) $time = "+24 hour"; else $time = "+1 hour";
				
				if($result["blokovan"] != 1){				
					Cookies::set(
						array(
							"id"			=> $result["id"],
							"user" 			=> $result["jmeno"],
							"permission"	=> $result["prava"],
							"time"			=> time()
						),
						$time, false);
				}else{
					$error[] = "Váš učet je zablokován";
				}
			}
			else
				$error[] = "Bylo zadáno špatné heslo";
		}
		
		if(count($error) == 0) 
			return true; 
		else 
			return $error;
	}
	
	public static function current(){
		if(Cookies::exists("id")){
			$id = $_COOKIE["id"];
			$result = dibi::query('SELECT * FROM :prefix:users WHERE id = %s', $id);			
			if(count($result) != 0){
				$result = $result->fetch();				
				return array(
						"id" 			=> $_COOKIE["id"],
						"nick" 			=> $result["nick"],
						"password"		=> $result["heslo"],
						"permission" 	=> $result["prava"],
						"time" 			=> $_COOKIE["time"],
						"avatar"		=> $result["avatar"],
						"background"	=> $result["background"],
						"email"			=> $result["email"],
						"ip"			=> $result["ip"],
						"data"			=> $result["data"],
						"recovery"		=> $result["recovery"],
						"passlastchange"=> $result["passlastchange"]
					);
			}else
				return false;
		}else
			return false;
	}
	
	public static function get($id){
		$result = dibi::query('SELECT * FROM :prefix:users WHERE id = %s', $id, ' OR jmeno = %s', $id);			
		if(count($result) != 0){
			$result = $result->fetch();		
			if($result["avatar"] == "") $result["avatar"] = "default.jpg";
			return array(
					"id" 			=> $result["id"],
					"nick" 			=> $result["nick"],
					"login" 		=> $result["jmeno"],
					"permission" 	=> $result["prava"],
					"time" 			=> time(),
					"avatar"		=> $result["avatar"],
					"background"	=> $result["background"],
					"email"			=> $result["email"],
					"ip"			=> $result["ip"],
					"data"			=> $result["data"],
					"recovery"		=> $result["recovery"],
					"passlastchange"=> $result["passlastchange"]
				);
		}else
			return false;
	}
	
	public static function setRecovery($id){
		$user = User::get($id);
		if(!$user)
			return false;
		else{
			if($user["recovery"] == ""){
				$key = Strings::random(8,Strings::$NUMBERS);
				$arr = array("recovery" => $key);
				dibi::query('UPDATE :prefix:users SET ', $arr, 'WHERE `id`=%s', $id, " OR `jmeno`=%s", $id);
				return $key;
			}else{
				return -5;
			}
		}		
	}
	
	public static function permission($id){
		$result = dibi::query('SELECT * FROM :prefix:permission WHERE id = %s', $id);			
		if(count($result) != 0){
			$result = $result->fetch();		
			return array(
					"id" 			=> $result["id"],
					"level" 		=> $result["level"],
					"name" 			=> $result["name"],
					"color" 		=> $result["color"],
					"image"			=> $result["image"],
					"permission"	=> Config::sload($result["data"])
				);
		}else
			return false;
	}
	
	public static function permission_set(){
		
	}
}