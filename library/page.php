<?php
class Page {
	public $title = "Title of your page";
	public $styles = null;
	public $script = null;
	public $config = null;
	
	private $root;
	 
	public function __construct($root){
		$this->root = $root;
		$this->config = array(
						"Autor" 		=> $root->config->get("autor"),
						"Description" 	=> $root->config->get("description"),
						"Charset" 		=> "UTF-8",
						"Title" 		=> $root->config->get("title"),
						"Keywords" 		=> $root->config->get("keywords")
					);
		$this->title = $root->config->get("title_head");
		$this->add_script($root->router->url . "include/jquery-1.11.2.min.js");
		$this->add_script($root->router->url . "include/natsu.js");		
	}
	
	public function head(){
		echo "<head>";
		echo '<title>';
			if($this->root->config->get("pre-title") != "") 
					echo $this->root->config->get("pre-title")."::";
			echo $this->config["Title"];
		echo '</title>';
		echo '<meta http-equiv="Content-Type" content="text/html; charset=' . $this->config["Charset"] . '">';
		echo '<meta name="description" content="' . $this->config["Description"] . '">';
		echo '<meta http-equiv="Content-language" content="' . $this->root->GLOBAL_LANGUAGE . '">';
		echo '<meta name="author" content="' . $this->config["Autor"] . '">';
		echo '<meta name="keywords" content="' . $this->config["Keywords"] . '">';
		echo '<link href="' . _ROOT_DIR . '/favicon.ico?cache='._CACHE.'" rel="icon">';
		if($this->script != null){
			foreach($this->script as $script){
				echo '<script type="text/javascript" src="' . $script . '?cache='._CACHE.'"></script>';
			}
		}
		if($this->styles != null){					
			foreach($this->styles as $style){
				echo '<link rel="stylesheet" type="text/css" href="' . $style . '?cache='._CACHE.'" media="screen" />';
			}
		}
		echo "</head>";
	}
	
	public function page_draw(){		
		$plugin = $this->root->module_manager->hook_call("page.".$this->root->router->_data["module"][0]);
		if( $plugin["called"] == 0){
			$this->draw_error("Module not a found!","Module with name \"".$this->root->router->_data["module"][0]."\" not a found! Please conact administrator of page thanks.");
		}else{
			echo $plugin["output"];
		}
	}
	
	public function error_box($text, $class){
		$error = $this->root->module_manager->hook_call("page.box.error");
		if($error["called"] == 0)
			echo "<div class='box ".$class."'>".$text."</div>";
	}
	
	public function draw_error($title, $text){
		echo "<h1>".$title."</h1>";
		echo $text;
	}
	
	public function menu_draw($name, $setting){
		$result = dibi::query("SELECT * FROM :prefix:menu WHERE box=%s", $name, " AND visible = 1 ORDER BY position");

		if(!isset($setting["noul"])){ 				$setting["noul"] = false; }
		if(!isset($setting["class"])){ 				$setting["class"] = "menu"; }
		if(!isset($setting["a_class"])){ 			$setting["a_class"] = ""; }
		if(!isset($setting["li_selected_class"])){ 	$setting["li_selected_class"] = "selected"; }
		if(!isset($setting["custom_tag"])){ 		$setting["custom_tag"] = "li"; }

		if( $setting["noul"]!=true ){ echo "<ul".($setting["class"]!=""?" class='".$setting["class"]."'":"").">"; }
		foreach ($result as $n => $row) {
				$this->menu_item_get($row, $setting);
		}
		if( $setting["noul"]!=true ){ echo "</ul>"; }
	}
	
	public function menu_item_get($row, $setting){
		if($row["data"]!=""){ $data = $this->root->config->load($row["data"]); }else{ $data = ""; }
		if($row["typ"] == "index"){
			echo "<".$setting["custom_tag"].
				($this->root->router->_get == ""?" class='".$setting["li_selected_class"]."'":"")
				."><a href='".$this->root->router->url."'".
				($setting["a_class"] != ""?" class='".$setting["a_class"]."'":"")
				.">".$row["title"]."</a></".$setting["custom_tag"].">";
		}
		else if($row["typ"] == "article"){
			if( $this->root->router->_data["module"][0]=="article" and ( $this->root->router->_data["id"][0] == $data["alias"] or $this->root->router->_data["id"][0] == $data["id"] ) ){ 
				$select = true;
			}else{ $select = false; }
			echo "<".$setting["custom_tag"].
				($select?" class='".$setting["li_selected_class"]."'":"")
				."><a href='".$this->root->router->url.$data["alias"]."'".
				($setting["a_class"] != ""?" class='".$setting["a_class"]."'":"")
				.">".$row["title"]."</a></".$setting["custom_tag"].">";
		}
		else if($row["typ"] == "category"){
			if( $this->root->router->_data["module"][0]=="category" and ( $this->root->router->_data["id"][0] == $data["alias"] or $this->root->router->_data["id"][0] == $data["id"] ) ){ 
				$select = true;
			}else{ $select = false; }
			echo "<".$setting["custom_tag"].
				($select?" class='".$setting["li_selected_class"]."'":"")
				."><a href='".$this->root->router->url."category/".$data["alias"]."'".
				($setting["a_class"] != ""?" class='".$setting["a_class"]."'":"")
				.">".$row["title"]."</a></".$setting["custom_tag"].">";
		}
		else if($row["typ"] == "url"){				
			if( $this->root->router->_get==$data["url"] ){ 
				$select = true;
			}else{ $select = false; }
			echo "<".$setting["custom_tag"].
				($select?" class='".$setting["li_selected_class"]."'":"")
				."><a href='".$this->root->router->url.$data["url"]."'".
				($setting["a_class"] != ""?" class='".$setting["a_class"]."'":"")
				.">".$row["title"]."</a></".$setting["custom_tag"].">";
		}
		else if($row["typ"] == "login"){
			echo "<".$setting["custom_tag"]."><span class=body>";
				if($row["title"] == "")
					echo "<b class=title>Přihlášení</b>";
				else
					echo "<b class=title>".$row["title"]."</b>";
					
				if(Cookies::exists("user") and Cookies::exists("id")){
					if(isset($_GET["logout"])){						
						Cookies::delete(array("id","user","permission"));
						header("location:./");
					}
					if(Cookies::security_check("permission") and Cookies::security_check("id")){
						$user = User::current();
						$perm = User::permission($user["permission"]);
						if($user["avatar"] == ""){ $user["avatar"] = $this->root->config->get_variable("avatar"); }
						echo "<img src='".Router::url()."/upload/avatars/".$user["avatar"]."' class=avatar>";
						echo "<div id=username class='".$perm["name"]."' title='".$perm["name"]."' style='color:".$perm["color"]."'>".$user["nick"]."</div>";
						echo "<a href='?logout'>Odhlásit se</a><a href='".Router::url()."settings/' class=left_separator>Nastavení</a>";
						if($perm["permission"]["admin"]==1){ echo "<a href='".Router::url()."admin/' class=left_separator>Admin</a>"; }
					}else{
						echo "<div style='color:red;font-weight:bold;'>COOKIES SECURITY ERROR!</div>";
						header("location:?logout");
					}
				}else{					
					if(isset($_POST["login"])){
						$username = $_POST["username"];
						$password = $_POST["password"];
						if(isset($_POST["remember"])) $remember = true; else $remember = false;				
						
						$login = User::login($username, $password, $remember);
						if(!is_array($login)){
							$this->error_box("Přihlášen", "ok");
							header("location:".Router::url(true));
						}else{	
							$mess="";
							for($i = 0;$i < count($login); $i++){
								$mess.= "<div>".$login[$i]."</div>";
							}
							$this->error_box($mess, "error");
						}
					}
					
					echo "<form action=# method=post class=loginbox><table>";	
						echo "<tr><td style='width:60%;'>Jméno</td><td><input type=text name=username value=''></td></tr>";
						echo "<tr><td>Heslo</td><td><input type=password name=password value=''></td></tr>";
						echo "<tr><td colspan=2 style='text-align:right;'><input type=checkbox name=remember value='1'> Zapamatovat</td></tr>";
						echo "<tr><td colspan=2 style='text-align:center'><input type=submit class=loginbutton name=login value='Přihlásit se'>";
						if($data["register"]=="1"){
							if( $this->root->router->_get=="register" ){ $select = true; }else{ $select = false; }							
							echo " <a href='".$this->root->router->url."register' class='registerlink ".($select?" ".$setting["li_selected_class"]:"")."'>Registrovat se</a><br style='clear:right;'>";
						}
						if( $this->root->router->_get=="newpass" ){ $select = true; }else{ $select = false; }
						echo " <a href='".$this->root->router->url."newpass' onClick=\"return recoveryPass(1);\" class='newpasslink ".($select?" ".$setting["li_selected_class"]:"")."'>Zapomenuté heslo</a>";
						
						echo "</td></tr>";
					echo "</table></form>";	
				}
			echo "</span></".$setting["custom_tag"].">";
		}
		else{
			// Use module menu
		}
	}
	
	public function template_parse($filename, $variables = NULL){
		$name = explode("/", $filename);
		$fnam = end($name);
		$name = explode(".", $fnam);
		$name = str_replace(".".end($name),"",$fnam);
		$html = file_get_contents($filename);
		$html = preg_replace_callback(
        '/\{\$(.*)\}/',
        function ($matches) use ($variables) {            
				return "<?php echo $".$matches[1]."; ?>";
        },
        $html);		
		$html = preg_replace_callback(
        '/\{\?(.*)\}/',
        function ($matches) use ($variables) {									
			return "<?php ".$matches[1]." ?>";
        },
        $html);
		$hash = sha1($html);
		$file = _ROOT_DIR."/temp/templates/".$this->root->router->_data["module"][0].".".$name.".".$hash.".template.php";
		if(!file_exists($file))
			file_put_contents($file, $html);
		include($file);
		return "";
	}
	
	public function add_style($url){
		$this->styles[] = $url;
	}
	
	public function add_script($url){
		$this->script[] = $url;
	}
}