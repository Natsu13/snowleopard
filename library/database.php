<?php
class Database {
	private $config 		= null;
	private $database_name 	= "";
	private $encode 		= "utf8";
	private $connect;
	public  $isConnected 	= false;
	public  $prefix 		= "";
	private $root;
	
	public function __construct($root){
		$this->root = $root;
		
		include _ROOT_DIR . "/config/db.php";
		include _ROOT_DIR . "/include/dibi/dibi.min.php";
		
		$this->config = array(
							"server" 	=> $database["server"],
							"user" 		=> $database["login"],
							"password" 	=> $database["password"]
						);
		if( $database["prefix"] != "" ){ $database["prefix"] = $database["prefix"]."_"; }
		$this->prefix = $database["prefix"];
		$this->database_name = $database["database"];			
	}
	
	public function connect($global = true){	
		$connection = false;
		
		$options = array(
			'driver'   => 'mysql',
			'host'     => $this->config["server"],
			'username' => $this->config["user"],
			'password' => $this->config["password"],
			'database' => $this->database_name,
			'profiler' => array(
				'run' => TRUE,
				'file' => _ROOT_DIR . '/log/mysql.log.sql',
			),
			'charset'  => $this->encode
		);
		
		if($global){
			try {
				dibi::connect($options);
			}
			catch (DibiException $exception) {				
				$this->root->message[] = array( 
										"state"		=> $this->root->_MESSAGE_FATAL, 
										"message" 	=> "(DIBI Error) " . $exception->getMessage(),
										"execution_time" => round(microtime(true) - $this->root->time_start, 4)
									);
				$this->root->draw_log();
				die;
			}
			
			$connection = dibi::getConnection();
			$connection->onEvent[] = 	function(DibiEvent $e){ 
											if ($e->result instanceof Exception) {
												echo "<style>* {font-family: Arial;}</style>";
												$mess = "<div style='padding: 20px;padding-bottom: 0px;'>[".$e->result->getCode()."] ".$e->result->getMessage()."</div>";
												$mess.= "<div style='padding: 15px;border: 1px solid silver;margin: 20px;border-radius: 2px;background-color: #EDEDED;'>".$e->sql."</div>";
												$mess.= "<div style='padding: 15px;border: 1px solid silver;margin: 20px;border-radius: 2px;background-color: #EDEDED;'>".$e->source[0]." on line ".$e->source[1]."<br>".Utilities::getFileLines($e->source[0], $e->source[1])."</div>";
												echo Utilities::getFileLines($e->source[0], $e->source[1]);
												echo Utilities::fatal("Mysql error", $mess); 
												exit;
											}
										};
			
			$this->root->log("(DIBI Info)[LOG] Logovací soubor je umístěn zde '" . _ROOT_DIR . "/log/mysql.sql'");
			$this->root->log("(DIBI Info)[Global] Successfully connected to database \"" . $this->config["server"]  . ":" . $this->config["user"] . "@" . $this->database_name . "\" [Encode: " . $this->encode . "]");
		}
		else{
			$connection = new DibiConnection($options);
			$this->connect = $connection;
			$this->root->log("(DIBI Info)[Local] Successfully connected to database \"" . $this->config["server"]  . ":" . $this->config["user"] . "@" . $this->database_name . "\" [Encode: " . $this->encode . "]");
		}
		
		//Define prefix table
		//dibi::addSubst('prefix', $this->prefix);
		dibi::getSubstitutes()->prefix = $this->prefix;
		//Please use in query and other dotaz this format... 
		//dibi::query("SELECT * FROM [:prefix:items]");
		
		$this->isConnected = true;
		
		return $connection;
	}
	
	public function load(){
		$result = dibi::query('SELECT * FROM :prefix:settings');

		foreach ($result as $n => $row) {
			$this->root->config->set_variable($row["name"], $row["value"], ($row["protected"]==1?true:false));
		}
		
		//initial setting all variable
		$this->root->config->set_variable("title_head", $this->root->config->get("title"));
	}
	
	public static function getConfig($name){
		$result = dibi::query('SELECT * FROM :prefix:settings WHERE name=%s', $name)->fetch();		
		return $result["value"];
	}
	
	public function setEncode($encode = "utf8"){
		$this->encode = $encode;
	}
	
	public function getError(){
		return mysql_error();
	}
	
	public function getConnect(){
		return $this->connect;
	}
}