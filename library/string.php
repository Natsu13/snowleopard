<?php
class Strings {
	public $root;
	public static $NUMBERS = 2;
	
	public function __construct($root){
		$this->root = $root;
	}

	public static function sklonuj($n, $arr){
		if($n == 1)
			return $arr[0];
		else if($n >= 2 and $n <= 4)
			return $arr[1];
		else
			return $arr[2];
	}
	
	public static function random($length, $special=false) 
	{
		if($special == Strings::$NUMBERS)
			$mozne_znaky = '0123456789';
		elseif($special)
			$mozne_znaky = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ,.-ů§ú)?:_"!/(ˇ%0-+*/';
		else
			$mozne_znaky = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$vystup = '';
		$pocet_moznych_znaku = strlen($mozne_znaky);
		for ($i=0;$i<$length;$i++) {
			$vystup .= $mozne_znaky[mt_rand(0,$pocet_moznych_znaku-1)];
		}
		return $vystup;
	}
	
	public static function getMonth($val){
		switch($val){
			   case 1:$return ="leden";break;
			   case 2:$return ="únor";break;
			   case 3:$return ="březen";break;
			   case 4:$return ="duben";break;
			   case 5:$return ="květen";break;
			   case 6:$return ="červen";break;
			   case 7:$return ="červenec";break;
			   case 8:$return ="srpen";break;
			   case 9:$return ="září";break;
			   case 10:$return ="říjen";break;
			   case 11:$return ="listopad";break;
			   case 12:$return ="prosinec";break;
			   default:$return ="neznámý";break;
		}
		return $return;		 
	}
	
	public static function htmlStr($input){
		return str_replace(array("&", "<", ">", "\"", "'"), array("&amp;", "&lt;", "&gt;", "&quot;", "&#39;"), $input);
	}
	 
	public static function lower($input){
		return str_replace(array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"), array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"), $input);
	} 
	
	public static function upper($input){
		return str_replace(array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"), array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"), $input);
	} 
	 
	public static function strHtml($input, $double = false)
	{ 
		$output = str_replace(array("&lt;", "&gt;", "&quot;", "&#39;", "&amp;"), array("<", ">", "\"", "'", "&"), $input);
		if($double) {
			$output = this.strHtml($output);
		}
		return $output;
	}
	
	public static function undiacritic($in, $na="-"){
		$in = preg_replace('~[^\\pL0-9_]+~u', $na, $in);
		$in = trim($in, $na);
		$in = iconv("utf-8", "us-ascii//TRANSLIT", $in);
		$in = strtolower($in);
		$out = preg_replace('~[^-a-z0-9_]+~', '', $in);
		return $out;
	}
	
	public static function str_time($csp, $noconvert=true, $without_time=false)
	{
		$posun = 0;
		$den=Date("j", $csp + ($posun * 3600)); 
		$mes=Date("m", $csp + ($posun * 3600)); 
		$rok=Date("Y", $csp + ($posun * 3600)); 
		$hod=Date("H", $csp + ($posun * 3600)); 
		$min=Date("i", $csp + ($posun * 3600)); 
		$sec=Date("s", $csp + ($posun * 3600));
		
		if($without_time){return $den.". ".Strings::getMonth($mes)." ".$rok;}
		
		if(!$noconvert){return $den.". ".Strings::getMonth($mes)." v ".$hod.":".$min."";}	 
		
		if(Date("Y", time())==$rok and Date("j", time())==$den and Date("m", time())==$mes)
			$vr = "Dnes v ".$hod.":".$min;
		elseif(Date("Y", time())==$rok and Date("j", time())==($den+1) and Date("m", time())==$mes)
			$vr = "Včera v ".$hod.":".$min;
		elseif(Date("Y", time())==$rok and Date("j", time())==($den-1) and Date("m", time())==$mes)
			$vr = "Zítra v ".$hod.":".$min; 
		elseif(Date("Y", time())==$rok)
			$vr = $den." ".Strings::getMonth($mes)." v ".$hod.":".$min;		
		else
			$vr = $den.". ".Strings::getMonth($mes)." ".$rok;
		return "<span title='".$den.".".$mes.".".$rok.", ".$hod.":".$min."'>".$vr."</span>";
	}
	
	function json_encode_my($array){
		$return = "";
		$onlyNumber = 1;
		foreach($array as $key => $value){
			if(gettype($key)!="integer"){ $onlyNumber=0; }
		}
		$i=0;$a=0;
		$return.=($onlyNumber==1?"[":"{");
		foreach($array as $key => $value){
			if(is_array($value)){ 
				if($key===$a){ 
					$return.=json_encode_my($value); 
				}else{ 
					$return.='"'.$key.'":'.json_encode_my($value); 
				} 
			}
			else if(($value==$a and gettype($value)=="integer" and $key=="") or ($key==$a and gettype($key)=="integer")){ 
				if(gettype($value)=="integer"){ 
					$return.=$value; 
				}else{ 
					$return.='"'.$value.'"';
				} 
			}
			else{ 
				if(gettype($value)=="integer"){ 
					$return.='"'.$key.'":'.$value; 
				}else{ 
					$return.='"'.$key.'":"'.$value.'"'; 
				} 
			}
			if($a!=count($array)-1){$return.=",";}
			$a++;
		}
		$return.=($onlyNumber==1?"]":"}");
		return $return;
	}
}