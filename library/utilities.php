<?php
class Utilities {
	public $root;
	
	public function __construct($root){
		$this->root = $root;
	}
	
	public static function captcha($name){
		$text = Strings::upper(Strings::random(5));
		Cookies::set("captcha_".$name,$text);
		return Images::text($text,array(105,24),21); 
	}
	
	public static function captcha_test($name, $value){
		if(!isset($_COOKIE["captcha_".$name])) return false;
		if($_COOKIE["captcha_".$name] == $value)
			return true;
		return false;	
	}
	
	public static function post_debug(){
		echo "<table style='table-layout: fixed; border-collapse: collapse;' border=1 class='snowLeopard'>";
		echo "<tr><th width=80>Type</th><th width=300>Key</th><th width=600>Value</th></tr>";
		foreach($_POST as $key => $value){
			echo "<tr><td>POST</td><td>".$key."</td><td>".$value."</td></tr>";
		}
		foreach($_GET as $key => $value){
			echo "<tr><td>GET</td><td>".$key."</td><td>".$value."</td></tr>";
		}
		if(count($_POST) == 0 and count($_GET) == 0)
			echo "<tr><td colspan=2>Žádné data...</td></tr>";
		echo "</table>";
	}
	
	public static function ip(){
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
			$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];		
		}else if(isset($_SERVER["HTTP_FORWARDED_FOR"])){
			$ip = $_SERVER["HTTP_FORWARDED_FOR"];		
		}else{
			$ip = $_SERVER["REMOTE_ADDR"];
		}
		if($ip == "::1"){ $ip = "127.0.0.1"; }
		if(strpos($ip, ",") !== false){
			$ip = explode(",", $ip);
			return trim($ip[0]);
		}else return $ip;
	}
	
	public static function isEmail($email){
		if(filter_var($email, FILTER_VALIDATE_EMAIL)){
			return true;
		}else{
			return false;
		}
	}
	
	public static function fatal($t, $m){
		echo "<div style='position:fixed;top:0px;left:0px;width:100%;height:100%;background-color:white;' id=error>";
			echo "<h1 style='background-color: red;padding: 13px;margin: 0px;color: white;' onClick=\"$('#error').hide();\">".$t."</h1>";
			echo "<div>".$m."</div>";
			echo "<div style='position:absolute;bottom:0px;left:0px;border-top: 1px solid silver;width: 100%;background: #E8E8E8;font-size:11px;'><div style='padding: 7px;'>";
				echo "Report generate at <b>".Date("j/m/Y H:i:s", Time())."</b><br>";
				if(!isset($_GET["url"])){ $url = ""; }else{ $url = $_GET["url"]; }
				if(substr($url, -1) == "/"){ $url = substr($url, 0, -1); }
				if(substr($_SERVER["REQUEST_URI"], -1) != "/"){ $_SERVER["REQUEST_URI"].="/"; }
				if(isset($_SERVER["REQUEST_SCHEME"])){$http=$_SERVER["REQUEST_SCHEME"];}else{$http="http";}
				$url = $http . "://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
				echo "Location <b>".$url."</b><br>";
				echo "PHP Version <b>".phpversion()."</b><br>";
				echo "Number of mysql comands <b>".(dibi::$numOfQueries)."</b> ( Time <b>".(dibi::$totalTime)."s</b> )<br>";
				echo "SnowLeopard RS Debuger ver. <b>1.0.0</b>";
			echo "</div></div>";
		echo "</div>";
	}
	
	public static function getFileLines($file, $line){
		$line--;
		$file_ = file_get_contents($file);
		$radk = explode("\n", $file_);
		$start = $line-4;;
		$end = $line+5;
		if($start < 0){
			$start = 0;
			$end = 8;
		}
		$return = "<table style='table-layout: fixed; border-collapse: collapse;'>";
		for($i = $start; $i < $end; $i++){
			$code = htmlspecialchars($radk[$i]);
			if(substr(trim($radk[$i]), 0, 2) == "//"){
				$c="green"; 
			}else{
				$c="";
				$code = str_replace("echo", "<b>echo</b>", $code);
			}
			
			$return.="<tr".($i==$line?" bgcolor=red style='color:white;font-weight:bold;'":"")."><td style='padding:3px;padding-right: 7px;text-align: right;background: silver;'><b>".$i.".</b> </td><td style='color:".$c.";padding:3px;'>".str_replace("\t","<span style='display:inline-block;width:20px;'></span>",$code)."</td></tr>";
		}
		$return.= "</table>";
		return $return;
	}
	
	public static function log($text, $data){
		$data = array(
					"text" 	=> $text,
					"date" 	=> time(),
					"ip"	=> Utilities::ip(),
					"user"	=> Utilities::ssave(User::current()),
					"data" 	=> ($data==""?"":Utilities::ssave($data))
				);
		dibi::query('INSERT INTO :prefix:log', $data);
		return dibi::InsertId();
	}
	
	public static function select($data, $sel = "", $name = "", $style = ""){
		$output = "<select name='".$name."' style='".$style."'>";
		foreach($data as $key => $value){			
			$output.="<option value='".$key."'".($sel==$key?" selected":"").">".$value."</option>";
		}
		$output.="</select>";
		return $output;
	}
}