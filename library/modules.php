<?php
/* Class for malipuation with modules */
class Modules {
	private $modules = NULL;
	private $Modules_loaded = 0;
	public $root;
	public $router;
	
	private $actual_module_name = "";
	
	function __construct($root){
		$this->root = $root;
		$this->router = $this->root->router;
		$adresar = opendir(_ROOT_DIR . "/modules/");
		while ($dir = readdir($adresar)){			
			if( is_dir( _ROOT_DIR . "/modules/" . $dir ) and $dir!="." and $dir!=".." ){
				if(file_exists(_ROOT_DIR . "/modules/" . $dir . "/" . $dir . ".config.php")){
					include_once(_ROOT_DIR . "/modules/" . $dir . "/" . $dir . ".config.php");						
									
					$this->modules[$modules[$dir]["Name"]] = array(
													"Dir" => _ROOT_DIR . "/modules/" . $dir . "/",
													"CName" => $modules[$dir]["Code_name"],
													"Enable" => 1
												);
					
					$root->message[] = array( 
								"state"		=> $root->_MESSAGE_INFO, 
								"message" 	=> "Load module(".$modules[$dir]["Name"].") configuration \"". $dir ."\"(" . _ROOT_DIR . "/modules/" . $dir . ".config.php)",
								"execution_time" => round(microtime(true) - $root->time_start, 4)
							);
					
					$root->message[] = array( 
								"state"		=> $root->_MESSAGE_OK, 
								"message" 	=> "Loading module file \"". $dir ."\"(" . _ROOT_DIR . "/modules/" . $dir . ".php)",
								"execution_time" => round(microtime(true) - $root->time_start, 4)
							);

					$this->actual_module_name = $modules[$dir]["Name"];
					include_once(_ROOT_DIR . "/modules/" . $dir . "/" . $dir . ".php");
					
					$this->Modules_loaded++;
				}else{				
					$root->message[] = array( 
								"state"		=> $root->_MESSAGE_WARNING, 
								"message" 	=> "Module config file is missing! (".$dir.")",
								"execution_time" => round(microtime(true) - $root->time_start, 4)
							);
				}
			}
		}
		
		/* Seřazení podle priority */
		foreach($this->modules as $mod){
			ksort($mod);
		}
		
		if($this->Modules_loaded == 0){
			$m = "No modules found in dir: " . _ROOT_DIR . "/modules/";
		}else{
			$m = "Successfully initialize modules: ".$this->Modules_loaded;
		}
		
		$root->message[] = array( 
							"state"		=> $root->_MESSAGE_INFO, 
							"message" 	=> $m,
							"execution_time" => round(microtime(true) - $root->time_start, 4)
						);
	}
	
	/*
	This is simple hook funkcions
	*/
	private $hooks = NULL;
	public  $global_count_hook_called = 0;
	
	function hook_register($hook_name, $function_name, $priotiry = 0){
		if($priotiry < -10){ $priotiry = -10; }else if($priotiry > 10){ $priotiry = 10; }
		$this->hooks[$hook_name][$priotiry][$this->actual_module_name][] = $function_name;
		ksort($this->hooks[$hook_name]);
	}
	
	function hook_call($hook_name, $data = NULL){
		$called = 0;
		$output = "";
		$data[] = $this;
		$data[] = &$output;
		if(!isset($this->hooks[$hook_name])){ 
			$this->hooks[$hook_name] = NULL; 
			$this->root->log("Create empty hook: ".$hook_name);
		} 
		if(count($this->hooks[$hook_name]) > 0){
			foreach($this->hooks[$hook_name] as $hooks_p){
				foreach($hooks_p as $m_name => $hooks){
					$this->root->log_called = $hook_name."::".$m_name;
					if($this->modules[$m_name]["Enable"] == 1){
						foreach($hooks as $hook){
							call_user_func_array($hook, $data);
							$called++;
						}
					}
				}	
			}
		}
		
		$this->root->log_called = "";
		if($called!=0){
			$this->root->message[] = array( 
							"state"		=> $this->root->_MESSAGE_INFO, 
							"message" 	=> "Successfully called hooks: " . $hook_name."(".$called.")",
							"execution_time" => round(microtime(true) - $this->root->time_start, 4)
						);
		}
		$this->global_count_hook_called += $called;
		return  ["output" => $output, "called" => $called];
	}
	
	/*
	This is simple page module manager
	There is all page registered
	*/
	
	function setState($module, $state){
		foreach($this->modules as $mods){
			foreach($mods as $mod){
				if($mod["CName"] == $module){
					$mod["Enable"] = $state;
					
					if($state == 1){ $m = "Enabling";$s = $this->root->_MESSAGE_OK;  }else{ $m="Disabling";$s = $this->root->_MESSAGE_ERROR; }
					
					$this->root->message[] = array( 
							"state"		=> $s, 
							"message" 	=> $m . " module \"" . $module . "\"(" . $mod["Name"] . ")",
							"execution_time" => round(microtime(true) - $this->root->time_start, 4)
						);
				}
			}
		}
	}
	
	function enable($module){ $this->setState($module, 1); }
	function disable($module){ $this->setState($module, 0); }
}