<style>
	* { padding: 0px; margin: 0px; font-family: Arial; }
	.topbar {
		background-color: #E2E2E2;
		background: linear-gradient(#C1C1C1 9%, #E2E2E2);
		padding: 0px 10px;
		box-shadow: 0px 1px 8px black;
	}
	.topbar ul {
		list-style: none;
		float:right;
	}
	.topbar ul li {
		display: inline-block;
		padding: 4px;
		padding-right: 15px;
	}
	.topbar ul li a {
		text-decoration: none;
		color: black;
	}
	.topbar b {
		padding: 4px;
		display: inline-block;
	}
	.content {
		width: 1000px;
		margin: 0px auto;
		padding: 10px 0px;
	}
</style>

<div class="topbar">
	<b>SnowLeopard instalační skript</b>
	<ul>
		<li><a href=#>Nastavení</a></li>
		<li><a href=#>Jazyk</a></li>
	</ul>
	<div style="clear:both;"></div>
</div>
<div class="content">
	<h1 style="margin:30px 0px;">Instalace redakčního systému SnowLeopard</h1>
	<div style="width: 100%;margin: 10px 0px;border: 1px solid silver;padding: 10px;height: 250px;overflow: auto;background:white;background:repeating-linear-gradient(#ECECEC 0%, white 3%, white 97%, #ECECEC 100%);">
		<h3>Podmínky pro provozování redakčního systému SnowLeopard</h3><br>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec urna gravida, vehicula ipsum sit amet, accumsan neque. Mauris vestibulum arcu ante, eget convallis lacus facilisis vel. Phasellus eu orci eu metus tincidunt hendrerit quis vel ante. Aliquam sit amet erat eget eros varius volutpat sed vel turpis. Vestibulum tempor sollicitudin ligula sit amet accumsan. Nunc tincidunt nec mauris vitae fermentum. Suspendisse non ante quam. Proin scelerisque blandit finibus. Pellentesque erat lacus, rhoncus vitae lobortis tempus, tristique sit amet libero. Fusce finibus dictum felis. Cras ornare rhoncus orci, et elementum leo dignissim nec. Morbi arcu dolor, pellentesque eget porta non, sollicitudin convallis nisi. Sed iaculis id nulla in placerat. Nam enim augue, pharetra vitae metus suscipit, malesuada sollicitudin neque. Nullam tempor lacus ut enim elementum pharetra.
<br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce nec urna gravida, vehicula ipsum sit amet, accumsan neque. Mauris vestibulum arcu ante, eget convallis lacus facilisis vel. Phasellus eu orci eu metus tincidunt hendrerit quis vel ante. Aliquam sit amet erat eget eros varius volutpat sed vel turpis. Vestibulum tempor sollicitudin ligula sit amet accumsan. Nunc tincidunt nec mauris vitae fermentum. Suspendisse non ante quam. Proin scelerisque blandit finibus. Pellentesque erat lacus, rhoncus vitae lobortis tempus, tristique sit amet libero. Fusce finibus dictum felis. Cras ornare rhoncus orci, et elementum leo dignissim nec. Morbi arcu dolor, pellentesque eget porta non, sollicitudin convallis nisi. Sed iaculis id nulla in placerat. Nam enim augue, pharetra vitae metus suscipit, malesuada sollicitudin neque. Nullam tempor lacus ut enim elementum pharetra.
<br><br>
Donec lorem lectus, tempus vel dapibus id, sodales at nisi. Donec in urna purus. Aliquam efficitur aliquet pellentesque. In sed dolor ac ex faucibus vehicula non eu ligula. Etiam euismod dignissim massa, iaculis sodales sapien maximus non. Aenean vehicula ut nibh non rhoncus. Nullam ac suscipit massa.
<br><br>
Quisque sit amet bibendum nulla. Phasellus varius sapien sit amet enim finibus, et placerat lectus imperdiet. Nam commodo leo non sem commodo tristique. Praesent rutrum ante tristique ante condimentum, sed fermentum quam vehicula. Sed sed varius purus. Mauris eget tempus tellus, et interdum dui. Donec vehicula augue neque, scelerisque faucibus tortor interdum a. Curabitur nulla lorem, dapibus ac posuere vitae, congue quis ex. Aenean egestas malesuada ante, non molestie justo bibendum non. Ut dictum pulvinar eleifend. Proin ac sollicitudin enim. Nunc et ultricies est. Etiam egestas commodo vulputate. Integer fermentum sem elit, at facilisis leo gravida a. Sed vitae feugiat metus. Maecenas lacus lectus, luctus at convallis eget, hendrerit vitae tellus.
<br><br>
Nam in dui condimentum, tincidunt elit eu, blandit massa. Duis erat justo, ornare sed facilisis a, condimentum quis est. Praesent elementum enim egestas, tincidunt elit sit amet, mollis eros. Maecenas congue, odio eu facilisis pretium, nisl nisl lacinia nibh, in auctor orci sem eget massa. Aenean purus quam, pulvinar vel erat sit amet, iaculis suscipit nisl. Nunc blandit tristique pellentesque. Maecenas consectetur ornare fringilla.
<br><br>
Nulla non tempor mi. Proin at mi euismod lorem aliquet venenatis vitae eu mauris. Donec hendrerit diam non turpis tincidunt, a aliquet neque luctus. Cras consequat, sapien in malesuada posuere, diam ipsum congue ex, viverra blandit velit mauris ac nulla. Ut at mauris eu diam semper convallis. Proin quam mauris, sodales a felis quis, varius feugiat enim. Duis pulvinar ultricies justo, quis laoreet nulla imperdiet id. Sed at ex faucibus, fermentum magna ut, fermentum lacus. Cras non diam sollicitudin, laoreet mi id, sagittis odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse eu magna non dui ultrices condimentum. Vestibulum condimentum vehicula cursus. Duis quis tellus posuere, congue dolor eleifend, egestas nulla. Ut interdum, nibh at feugiat fermentum, tellus velit luctus dolor, in ultricies neque urna sit amet est. Proin id elit dui. Morbi consectetur aliquam odio, non vehicula lorem semper rhoncus.
Donec lorem lectus, tempus vel dapibus id, sodales at nisi. Donec in urna purus. Aliquam efficitur aliquet pellentesque. In sed dolor ac ex faucibus vehicula non eu ligula. Etiam euismod dignissim massa, iaculis sodales sapien maximus non. Aenean vehicula ut nibh non rhoncus. Nullam ac suscipit massa.
<br><br>
Quisque sit amet bibendum nulla. Phasellus varius sapien sit amet enim finibus, et placerat lectus imperdiet. Nam commodo leo non sem commodo tristique. Praesent rutrum ante tristique ante condimentum, sed fermentum quam vehicula. Sed sed varius purus. Mauris eget tempus tellus, et interdum dui. Donec vehicula augue neque, scelerisque faucibus tortor interdum a. Curabitur nulla lorem, dapibus ac posuere vitae, congue quis ex. Aenean egestas malesuada ante, non molestie justo bibendum non. Ut dictum pulvinar eleifend. Proin ac sollicitudin enim. Nunc et ultricies est. Etiam egestas commodo vulputate. Integer fermentum sem elit, at facilisis leo gravida a. Sed vitae feugiat metus. Maecenas lacus lectus, luctus at convallis eget, hendrerit vitae tellus.
<br><br>
Nam in dui condimentum, tincidunt elit eu, blandit massa. Duis erat justo, ornare sed facilisis a, condimentum quis est. Praesent elementum enim egestas, tincidunt elit sit amet, mollis eros. Maecenas congue, odio eu facilisis pretium, nisl nisl lacinia nibh, in auctor orci sem eget massa. Aenean purus quam, pulvinar vel erat sit amet, iaculis suscipit nisl. Nunc blandit tristique pellentesque. Maecenas consectetur ornare fringilla.
<br><br>
Nulla non tempor mi. Proin at mi euismod lorem aliquet venenatis vitae eu mauris. Donec hendrerit diam non turpis tincidunt, a aliquet neque luctus. Cras consequat, sapien in malesuada posuere, diam ipsum congue ex, viverra blandit velit mauris ac nulla. Ut at mauris eu diam semper convallis. Proin quam mauris, sodales a felis quis, varius feugiat enim. Duis pulvinar ultricies justo, quis laoreet nulla imperdiet id. Sed at ex faucibus, fermentum magna ut, fermentum lacus. Cras non diam sollicitudin, laoreet mi id, sagittis odio. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse eu magna non dui ultrices condimentum. Vestibulum condimentum vehicula cursus. Duis quis tellus posuere, congue dolor eleifend, egestas nulla. Ut interdum, nibh at feugiat fermentum, tellus velit luctus dolor, in ultricies neque urna sit amet est. Proin id elit dui. Morbi consectetur aliquam odio, non vehicula lorem semper rhoncus.
	</div>
</div>