<?php
class Bootstrap {
	public $message = NULL;
	
	private $library = array("modules", "database", "config", "router", "page", "images", "string", "utilities", "cookies", "user");
	
	/* Message state variable */
	public	$_MESSAGE_LOG = -1,
			$_MESSAGE_INFO = 0,
			$_MESSAGE_OK = 1,
			$_MESSAGE_WARNING = 2,
			$_MESSAGE_ERROR = 3,
			$_MESSAGE_FATAL = 4;
	/* This variable provides the no info message write to base log file */
	private $_MINIMUM_LOGED_LEVEL = 1;
			
	public $base_log = "";
	/* Stop load system after fatal error in loading system library */
	public $exitOnFault = false;
	/* Number of loaded modules */
	public $Library_loaded = 0;
	
	/* Execution counter */
	public $time_start; 
	public $time_end;
	
	public $log_called = "";
	
	public $GLOBAL_LANGUAGE = "cs_CZ";
	
	public $template = "default";	// Template "furrbook"
	
	public function __construct( $bl ){		
		define("_CACHE", 1); 
		if($bl == "" or $bl == NULL){ $bl = _ROOT_DIR . "/log/base.log"; }
		$this->base_log = $bl;		
	}
	
	public function load(){
		if(!isset($_COOKIE["language"])){ $lang = $this->GLOBAL_LANGUAGE; }else{ $lang = $_COOKIE["language"]; }
		
		$this->time_start = microtime(true); 
		$this->load_library( $this->library ); 	
		
		Cookies::security_delete();//Check deleted security cookies
		
		$this->message[] = array( 
							"state"		=> $this->_MESSAGE_INFO, 
							"message" 	=> "Complete loaded libraries: " . $this->Library_loaded,
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
			
		$this->router = new Router($this);		
		$this->router->add("<module>[/<id>][/<page=1>]", "module=<module>&id=<id>&page=<page>");//Routa with lowest weight! Only to be used if no other route over it! Respectively below it.
		$this->router->add("<id>", "module=article&id=<id>");//Article route		
		//$this->router->add("forum-<id>[/<action=show>][/<page=1>]", "module=forum&id=<id>&action=<action>&page=<page>");		
			
		if(defined("_SYSTEM_LIBRARY_modules")){			
			$this->module_manager = new Modules($this);
			//$this->module_manager->call_module("database");
		}else{ 
			$this->message[] = array( 
							"state"		=> $this->_MESSAGE_FATAL, 
							"message" 	=> "Fatal error library \"modules\" is not defined!",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
			$this->draw_log();
			die();
		}  
		
		//Calling hook init
		$this->module_manager->hook_call("init");
		
		if(defined("_SYSTEM_LIBRARY_database")){
			$this->database = new Database($this);
			$this->database->connect();
		}else{
			$this->message[] = array( 
							"state"		=> $this->_MESSAGE_FATAL, 
							"message" 	=> "Fatal error library \"database\" is not defined!",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
			$this->draw_log();
			die();
		}
		
		//Calling hook with database
		$this->module_manager->hook_call("init.database");
		
		if(defined("_SYSTEM_LIBRARY_config")){
			$this->config = new Config($this);
			$this->config->set_variable("version", "0.1", true);	
			$this->config->set_variable("session_used", 0, false); //But we dont want it
			$this->database->load(); //Loading all confguration from database
			$this->log("Byla načtena konfigurace z databáze");
			date_default_timezone_set("UTC");
			$this->log("Časové pásmo bylo nastaveno na UTC+".$this->config->get("utc"));
		}else{ 
			$this->message[] = array( 
							"state"		=> $this->_MESSAGE_FATAL, 
							"message" 	=> "Fatal error library \"config\" is not defined!",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
			$this->draw_log();
			die();
		}  
		
		//Calling hook with loaded setting
		$this->module_manager->hook_call("init.setting");
		
		//If some module want it on
		if($this->config->get("session_used") == 1){
			session_start();
		}
		
		//Ajax route last
		$this->router->add("ajax/<module>[/<id>][/<page=1>]", "__type=ajax&module=<module>&id=<id>&page=<page>");
		$this->router->start();
		
		$this->module_manager->hook_call("page.".$this->router->_data["module"][0].".init.setting");		
		
		if(defined("_SYSTEM_LIBRARY_page")){
			$this->page = new Page($this);
		}else{
			$this->message[] = array( 
							"state"		=> $this->_MESSAGE_FATAL, 
							"message" 	=> "Fatal error library \"page\" is not defined!",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
			$this->draw_log();
			die();
		}
		
		$this->message[] = array( 
							"state"		=> $this->_MESSAGE_OK, 
							"message" 	=> "Initializate template \"". $this->template ."\"(" . _ROOT_DIR . "/templates/" . $this->template . "/init.php)",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
		include_once(_ROOT_DIR . "/templates/" . $this->template . "/init.php");
		
		if(!isset($_GET["__type"])){ $_GET["__type"] = "page"; }
		
		if($_GET["__type"] == "ajax"){
			$this->page->page_draw();
		}else{
			$this->page->head();// Show head of page
			
			$this->message[] = array( 
								"state"		=> $this->_MESSAGE_OK, 
								"message" 	=> "Loading template \"". $this->template ."\"(" . _ROOT_DIR . "/templates/" . $this->template . "/index.php)",
								"execution_time" => round(microtime(true) - $this->time_start, 4)
							);
			include_once(_ROOT_DIR . "/templates/" . $this->template . "/index.php");
		}
		
		$this->log_write( $this->message );
		$this->time_end = microtime(true);		
		
		$this->log("(DIBI Info) Celkový počet mysql příkazů: ".(dibi::$numOfQueries).", celkový čas v sekundách: ".(dibi::$totalTime));
		
		return round($this->time_end - $this->time_start, 4);
	}
	
	private function load_library($library = array("log")){
		//First load init system library
		foreach($library as $name){
			if(file_exists(_ROOT_DIR . "/library/" . $name . ".init.php")){
				$file =  _ROOT_DIR . "/library/" . $name . ".php";
				$this->message[] = array( 
							"state"		=> $this->_MESSAGE_OK, 
							"message" 	=> "Initializate library \"". $name ."\"(" . $file . ")",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
						
				include_once(_ROOT_DIR . "/library/" . $name . ".init.php");
			}
		}
		//Now load system library
		foreach($library as $name){
			$file =  _ROOT_DIR . "/library/" . $name . ".php";
			if(file_exists($file)){										
				include_once($file);
				define("_SYSTEM_LIBRARY_" . $name, true);		
						
				$this->message[] = array( 
							"state"		=> $this->_MESSAGE_OK, 
							"message" 	=> "Library \"". $name ."\"(" . $file . ") has successfully loaded",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
						
				$this->Library_loaded++;
			}else{
				$this->message[] = array( 
							"state"		=> $this->_MESSAGE_WARNING, 
							"message" 	=> "Failed to load library \"". $name ."\"(" . $file . ")",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
				if($this->exitOnFault){
					$this->message[] = array( 
							"state"		=> $this->_MESSAGE_ERROR, 
							"message" 	=> "Loading halted after fatal error!",
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
					break;	
				}
			}
		}
	}
	
	public function log($message){
		if($this->log_called != ""){ $m = "(" . $this->log_called . ") ".$message; }else{ $m=$message; }
		$this->message[] = array( 
							"state"		=> $this->_MESSAGE_LOG, 
							"message" 	=> $m,
							"execution_time" => round(microtime(true) - $this->time_start, 4)
						);
	}
	
	public function set_minimum_log_level( $level = 0 ){
		$this->_MINIMUM_LOGED_LEVEL = $level;
	}
	
	public function draw_log(){
		foreach( $this->message as $m ){
			if( $m["state"] == 0 ){ $c="blue"; }elseif( $m["state"] == 1 ){ $c="green"; }elseif( $m["state"] == 2 ){ $c="orange"; }elseif( $m["state"] == 3 ){ $c="red"; }elseif( $m["state"] == -1 ){ $c="silver"; }else{ $c="purple"; }
			echo "<span style='background-color:" . $c . ";padding: 0px;width: 6px;height: 14px;display: inline-block;margin-right: 3px;'> </span>";
			echo "<i style='color:silver;display: inline-block;width: 50px;'>".$m["execution_time"]."</i> ";	
			echo $m["message"] . "</font><br>";
		}
	}
	
	private function log_write( $log = NULL ){
		if($log != NULL){
			$soubor = fopen( $this->base_log, "a");
			foreach( $log as $data ){
				if( $data["state"] >= $this->_MINIMUM_LOGED_LEVEL )
				fwrite($soubor, Date("j/m/Y H:i:s", time()) . "||" . $_SERVER["REMOTE_ADDR"] . "||" . $data["state"] . "||" . $data["message"] . "\r\n");
			}
			fclose($soubor);
		}
	}
	
	public function draw_debug_box(){		
		if($_GET["__type"] == "page"){
			echo "<div id='debug_box' style='position:fixed;bottom: 0px;left:0px;right:0px;'>";
			echo "<div id='debug_button' style='background-color:black;color:white;padding: 10px 20px;float: right;right: 15px;position: relative;cursor:pointer;' onClick=\"togle_debug_box();\" title='Debug panel'> > </div>";
			echo "<div id=log style='background-color: black;color: white;padding: 20px;height: 350px;overflow: auto;width: 98%;'>";
			echo "<div style='border-bottom: 1px solid silver;margin-bottom: 7px;padding-bottom: 5px;'>LOG | <a href=# onClick=\"$(last).hide();last='#routing';$('#routing').show();\">ROUTING</a> | <a href=# onClick=\"$(last).hide();last='#cookies';$('#cookies').show();\">COOKIES</a> | <a href=# onClick=\"$(last).hide();last='#post';$('#post').show();\">POST/GET</a> | <b>DEBUG PANEL</b></div>";
			$this->draw_log();
			echo "</div>";
			echo "<div id=routing style='background-color: black;color: white;padding: 20px;height: 350px;display:none;overflow: auto;width: 98%;'>";
			echo "<div style='border-bottom: 1px solid silver;margin-bottom: 7px;padding-bottom: 5px;'><a href=# onClick=\"$(last).hide();last='#log';$('#log').show();\">LOG</a> | ROUTING | <a href=# onClick=\"$(last).hide();last='#cookies';$('#cookies').show();\">COOKIES</a> | <a href=# onClick=\"$(last).hide();last='#post';$('#post').show();\">POST/GET</a> | <b>DEBUG PANEL</b></div>";
			$this->router->draw_table();
			echo "</div>";
			echo "<div id=cookies style='background-color: black;color: white;padding: 20px;height: 350px;display:none;overflow: auto;width: 98%;'>";
			echo "<div style='border-bottom: 1px solid silver;margin-bottom: 7px;padding-bottom: 5px;'><a href=# onClick=\"$(last).hide();last='#log';$('#log').show();\">LOG</a> | <a href=# onClick=\"$(last).hide();last='#routing';$('#routing').show();\">ROUTING</a> | COOKIES | <a href=# onClick=\"$(last).hide();last='#post';$('#post').show();\">POST/GET</a> | <b>DEBUG PANEL</b></div>";
			Cookies::dump();
			echo "</div>";
			echo "<div id=post style='background-color: black;color: white;padding: 20px;height: 350px;display:none;overflow: auto;width: 98%;'>";
			echo "<div style='border-bottom: 1px solid silver;margin-bottom: 7px;padding-bottom: 5px;'><a href=# onClick=\"$(last).hide();last='#log';$('#log').show();\">LOG</a> | <a href=# onClick=\"$(last).hide();last='#routing';$('#routing').show();\">ROUTING</a> | <a href=# onClick=\"$(last).hide();last='#cookies';$('#cookies').show();\">COOKIES</a> | POST/GET | <b>DEBUG PANEL</b></div>";
			Utilities::post_debug();
			echo "</div>";
			echo "</div>";
			echo "<script>";
			echo "var last = '#log';";
			echo "function togle_debug_box(){";
			echo "	if($(\"#debug_button\").html()==' &gt; '){";
			echo "		$(\"#debug_button\").html(' < ');";
			echo "		$('#debug_box').css('bottom','-390px');";
			echo "	}else{";
			echo "		$(\"#debug_button\").html(' > ');";
			echo "		$('#debug_box').css('bottom','0px');";
			echo "	}";
			echo "}";
			echo "togle_debug_box();";
			echo "</script>";
		}
	}
}