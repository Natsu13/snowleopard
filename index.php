<?php
define("_ROOT_DIR", str_replace("\\", "/", getcwd()));

global $boot;

include _ROOT_DIR . "/include/bootstrap.php";
$boot = new Bootstrap("");
$boot->load();
$boot->draw_debug_box();
?>