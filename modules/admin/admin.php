<?php
$this->hook_register("init.setting", "admin_init", -10); 
$this->hook_register("page.admin.init.setting", "admin_init_setting", -10);
$this->hook_register("page.admin", "admin_page_draw", 0);

function admin_init($t){
	$t->router->add("admin[/<adminModule=info>][/<action=show>][/<id=1>]", "module=admin&admin_module=<adminModule>&action=<action>&id=<id>");	
}

function admin_init_setting($t){
	$t->root->config->set("pre-title","Administrace");
	$t->root->config->set("style.menu.left", "hide");
}

function admin_page_draw($t, &$output){	
	$user = User::current();
	$perm = User::permission($user["permission"])["permission"];
	echo "<h1 style='background-color: #2997CA;margin: 0px;padding: 12px 11px;color: white;font-size: 25px;'>Administrace</h1>";
	if($perm["admin"] == 1){
		echo "<div class=admin>";
			echo "<ul class=topmenu style='  z-index: 0;position: relative;'>";
					echo "<li ".($t->router->_data["admin_module"][0] == "info"?"class=select":"")."><a href='".$t->router->url_."admin/'>Informace</a></li>";
					echo "<li ".($t->router->_data["admin_module"][0] == "article"?"class=select":"")."><a href='".$t->router->url_."admin/article/'>Články</a></li>";
			echo "</ul>";
			include _ROOT_DIR."/modules/admin/modules/".$t->router->_data["admin_module"][0].".php";
		echo "</div>";
	}else{
		$t->root->page->error_box("Nemáš oprávnění pro vstup do aministrace!", "error");
	}
}