<?php
$action = $t->router->_data["action"][0];

echo "<ul class='topmenu sub'>";
	echo "<li ".($action == "show"?"class=select":"")."><a href='".$t->router->url_."admin/article/'>Seznam</a></li>";
	echo "<li ".($action == "new"?"class=select":"")."><a href='".$t->router->url_."admin/article/new/'>Napsat nový</a></li>";
echo "</ul>";

if($action == "show"){
	echo "<a href=# onClick='setHomePage();return false;'>Zvolit hlavní článek</a>";
	echo "<table class='tablik'>";
	echo "<tr><th width=350>Titulek</th><th width=120>Alias</th><th width=150>Datum vydání</th><th width=120>Akce</th></tr>";
	$result = dibi::query('SELECT * FROM :prefix:article');
	foreach ($result as $n => $row) {
		echo "<tr><td><b>".$row["title"]."</b></td><td>".$row["alias"]."</td><td>".Strings::str_time($row["date"])."</td><td>";
			//<a href='".$t->router->url_."admin/article/edit/".$row["id"]."' class=button>Upravit</a> <a href='#' class=button>Smazat</a>
			echo '<div style="float:left;" class=buttonline>';
			echo "<a href='".$t->router->url_."admin/article/edit/".$row["id"]."' class=button>Upravit</a>";
			echo '<a href="#" class="ContextMenu noarrow threedotmenu" dropdown="setting_article_'.$row["id"].'" dropdown-open="right" dropdown-absolute="false" style=""></a></div>';
			echo '<div class="listDiv" id="setting_article_'.$row["id"].'">';
				echo '<div class="listBox" style="width:160px;">';
					echo "<ul>";
						echo "<li><a href=#><img src='".Router::url()."/modules/admin/images/smaz.gif' class=des>Smazat článek</a></li>";
						echo "<li><a href=#><img src='".Router::url()."/modules/admin/images/delete.png' class=des>Zrušit publikování</a></li>";
						echo "<li class=cara></li>";
						echo "<li><a href=#><img src='".Router::url()."/modules/admin/images/question_blue.png' class=des>Převést na koncept</a></li>";
					echo "</ul>";
				echo "</div>";
			echo "</div>";
		echo "</td></tr>";
	}
	echo "</table>";
	?>
	<script>
	function setHomePage(){
		var d = new Dialog();
		d.setTitle('Zvolit hlavní článek');
		d.setButtons([Dialog.SAVE]);
		d.Load('<?php echo $t->root->router->url."ajax/dialog/setHomePage"; ?>');
	}
	</script>
	<?php
}
else if($action == "edit"){
	$result = dibi::query("SELECT * FROM :prefix:article WHERE id=%i", $t->router->_data["id"][0], " or alias=%s", $t->router->_data["id"][0])->fetch();
	if($result == NULL){
		$t->root->page->draw_error("Članek neexistuje", "Článek ".$t->router->_data["id"][0]." neexistuje!");
	}else{
		echo "<h1>Editace článku ".$result["title"]."</h1>";
		echo "<form method=post>";
		echo "<input type=submit name=edit value='Uložit článek'> <input type=submit name=koncept value='Uložit článek jako koncept'> <input type=submit name=copy value='Uložit článek jako kopii'>";
		
			echo "<table class=tabfor style='width:70%;margin:20px 0px;'>";
			echo "<tr><td width=100><label style='font-size:20px;'>Titulek</label></td><td width=430><input type=text style='font-size:20px;' name=title value='".$result["title"]."'></td></tr>";
			echo "<tr><td></td><td><b>Premanentní adresa: </b>".Router::url()." <input type=text name=alias style='width:250px;padding: 2px 7px;border: 0px;' value='".$result["alias"]."'> /</td></tr>";
			echo "<tr><td height=25></td></tr>";
			echo "<tr><td colspan=2><textarea name=text style='width:100%;' rows=20>".$result["text"]."</textarea></td></tr>";
			echo "</table>";

		echo "</form>";
	}
}