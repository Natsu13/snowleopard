<?php
$action = $t->router->_data["action"][0];

echo "<ul class='topmenu sub'>";
	echo "<li ".($action == "show"?"class=select":"")."><a href='".$t->router->url_."admin/info/'>Základní informace</a></li>";
	echo "<li ".($action == "new"?"class=select":"")."><a href='".$t->router->url_."admin/info/language/'>Jazyky</a></li>";
echo "</ul>";

if($action == "show"){
	if(isset($_POST["update"])){
		$pole = array(
					"title" => $_POST["title"],
					"description" => $_POST["description"],
					"autor" => $_POST["autor"],
					"keywords" => $_POST["keywords"],
					"utc" => $_POST["utc"]
				);
				
		foreach($pole as $key => $value){
			dibi::query('UPDATE :prefix:settings SET ', array("value" => $value), "WHERE `name`=%s", $key);
			$t->root->config->set($key, $value);
		}
		/*
		dibi::query('UPDATE :prefix:settings SET ', array("value" => $_POST["description"]), "WHERE `name`='description'");
		dibi::query('UPDATE :prefix:settings SET ', array("value" => $_POST["autor"]), "WHERE `name`='autor'");
		*/
		$t->root->page->error_box("Změny byly uloženy.", "ok");
	}
	$user = User::current();
	$perm = User::permission($user["permission"]);
	
	echo "<form method=post><table class=tabfor style='width:100%;'>";
	echo "<tr><td width=170><label>Titulek</label></td><td width=430><input type=text name=title value='".$t->root->config->get("title")."'></td><td width=20></td><td width=200><label>Webové stránky</label></td><td>".$t->router->url."</td></tr>";
	echo "<tr><td><label>Klíčová slova</label></td><td><input type=text name=keywords value='".$t->root->config->get("keywords")."'></td><td></td><td><label>Vaše IP Adresa</label></td><td>".Utilities::ip()."</td></tr>";
	echo "<tr><td></td><td><label><i>Pár slov popisující stránku oddělené čárkou</i></label></td><td></td><td><label>Oprávnění</label></td><td><span style='color:".$perm["color"]."'>".$perm["name"]."</span></td></tr>";
	echo "<tr><td><label>Popisek</label></td><td><input type=text name=description value='".$t->root->config->get("description")."'></td></tr>";
	echo "<tr><td><label>Autor</label></td><td><input type=text name=autor value='".$t->root->config->get("autor")."'></td></tr>";
	echo "<tr><td><label>Časové pásmo</label></td><td>".Utilities::select(
																		array(
																			"-12" => "UTC-12",
																			"-11" => "UTC-11",
																			"-10" => "UTC-10",
																			"-9" => "UTC-9",
																			"-8" => "UTC-8",
																			"-7" => "UTC-7",
																			"-6" => "UTC-6",
																			"-5" => "UTC-5",
																			"-4" => "UTC-4",
																			"-3" => "UTC-3",
																			"-2" => "UTC-2",
																			"-1" => "UTC-1",
																			"0" => "UTC",
																			"1" => "UTC+1",
																			"2" => "UTC+2",
																			"3" => "UTC+3",
																			"4" => "UTC+4",
																			"5" => "UTC+5",
																			"6" => "UTC+6",
																			"7" => "UTC+7",
																			"8" => "UTC+8",
																			"9" => "UTC+9",
																			"10" => "UTC+10",
																			"11" => "UTC+11",
																			"12" => "UTC+12",
																			"13" => "UTC+13",
																			"14" => "UTC+14",																			
																		),
																	$t->root->config->get("utc"),"utc","width:100%;padding:4px;")."</td></tr>";
	echo "<tr><td></td><td><label><i>Čas s časovým pásmem <b>UTC+".$t->root->config->get("utc")."</b> je <b>".date("Y-m-d H:i:s", time()+3600*$t->root->config->get("utc"))."</b></i></label></td><td></td></tr>";
	echo "</table>";
	echo "<div class=dialog-footer-button style='padding-left: 174px;margin-top:18px;float:initial;'><input type=submit class='blue button' name=update value='Upravit'></div>";
	echo "</form>";		
}
?>