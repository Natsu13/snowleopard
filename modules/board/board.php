<?php
$this->hook_register("init.setting", "board_init", -10);
$this->hook_register("page.board.init.setting", "board_title", -10);
$this->hook_register("page.board", "board_page_draw", 0);

function board_init($t){
	$t->router->add("board", "module=board");	
	$t->root->config->set("session_used",1);
	$t->root->log("========================================");
	$t->root->log("BOARD HAS BEEN LOADED SEE IT ON /board/!");
	$t->root->log("<font color=red>This module used session and curl!</font>");
	$t->root->log("========================================");
}

function board_title($t){
	$t->root->config->set("pre-title","Zeď");
}

function board_curl($url){
	global $PHPSEID ;
	$cSession = curl_init(); 
	curl_setopt($cSession,CURLOPT_URL,$url);
	curl_setopt($cSession,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($cSession,CURLOPT_HEADER, false); 
	curl_setopt($cSession,CURLOPT_COOKIEFILE, "./cookies/cookie_".$PHPSEID.".txt");
    curl_setopt($cSession,CURLOPT_COOKIEJAR, "./cookies/cookie_".$PHPSEID.".txt");
	curl_setopt($cSession,CURLOPT_USERAGENT,'Mobilní čtečka furry.cz');
	$result=curl_exec($cSession);
	curl_close($cSession);
	return $result;
}

function board_show($type){
	$PHPSEID = session_id();
		
	if($type == 1){
		echo "<div style='background-image:url(".Router::url()."/upload/lista_01.gif);height: 26px;background-repeat: no-repeat;background-position: -46px;border-bottom: 1px solid #5D5D5D;background-size: 223px 30px;'></div>";	
			
		$result=board_curl("http://www.furry.cz/index.php?s=f&a=t&p=1");
		preg_match_all('/\<tr id\=\"topic(.*)\" (.*)\>(.*)\<\/tr\>/U', $result, $matches, PREG_SET_ORDER);
		$maximum = 5;
		if(count($matches) > $maximum){ $maxim = $maximum; }else{ $maxim = count($matches); }
		for($i=0;$i<$maxim;$i++){
			preg_match_all('/\<td class\=\"(.*)\" (.*)\>(.*)\<\/td\>/U',  $matches[$i][3], $topics, PREG_SET_ORDER);
			$sta = preg_match_all('/\<img src\=\"(.*)\" alt\=\"(.*)\" title\=\"(.*)\" \/\>/U',  $topics[0][3], $states);
			$top = preg_match('/\<a href\=\"index\.php\?s\=f\&a\=rm\&t\=(.*)">(.*)\<\/a\>/U',  $topics[2][3], $topName);
			if($states[1][0]!="") { $states[1][0]="http://www.furry.cz/".$states[1][0]; }
			if(isset($states[1][1])) { $states[1][1]="http://www.furry.cz/".$states[1][1]; }else{ $states[1][1]=""; }
			$userTop[1]="";$userTop[2]="";
			$usa = preg_match('/\<span class\=\"user\">(.*)\<\/span\> \<span class\=\"date\"\>(.*)\<\/span\>/U',  $topics[4][3], $userTop);
			$topics[4][3] = iconv("ISO-8859-2","UTF-8",$topics[4][3]);
			
			if($topics[4][3]!="- nepřístupné -"){
				$new = preg_match('/(.*) \((.*)\)/U',  $topics[3][3], $news);
				if(count($news) == 0){ $news[1] = null; }
				if(substr($topics[3][3],0,1)=="("){ $news[1]=substr($topics[3][3],1,strlen($topics[3][3])-2);$news[2]=substr($topics[3][3],1,strlen($topics[3][3])-2); }
				elseif($news[1]==null){ $news[1]=$topics[3][3];$news[2]=0; }
				
				$topName[2] = iconv("ISO-8859-2","UTF-8",$topName[2]);
				$topics[1][3] = iconv("ISO-8859-2","UTF-8",$topics[1][3]);
				echo "<div class='olozkaid'>";
					echo '<div style="display:inline-block">';
					echo "<img src='".$states[1][0]."' style='padding:0px 4px;width:16px;'><br>";
					if($states[1][1]!="") echo "<img src='".$states[1][1]."' style='padding:0px 4px;width:16px;'>"; else echo "<div style='display: inline-block;width:16px;'></div>";
					echo '</div>';
					echo "<div style='padding: 7px;display: inline-block;width: 95%;'>".$topName[2];
					echo "<div style='float:right;text-align:right;'><b>".$userTop[1]."</b> ".$userTop[2]."<div class=mini><b>".$news[2]."</b> z ".$news[1]."</div></div>";
					echo "<div class=mini>".$topics[1][3]."</div></div>";
				echo "</div>";
				
			}else{
				$new = preg_match('/(.*) \((.*)\)/U',  $topics[3][3], $news);
				if(count($news) == 0){ $news[1] = null; }
				if(substr($topics[3][3],0,1)=="("){ $news[1]=substr($topics[3][3],1,strlen($topics[3][3])-2);$news[2]=substr($topics[3][3],1,strlen($topics[3][3])-2); }
				elseif($news[1]==null){ $news[1]=$topics[3][3];$news[2]=0; }			
				
				$topName[2] = iconv("ISO-8859-2","UTF-8",$topName[2]);
				$topics[1][3] = iconv("ISO-8859-2","UTF-8",$topics[1][3]);
				echo "<div class='olozkaid denbac'>";
					echo '<div style="display:inline-block">';
					echo "<img src='".$states[1][0]."' style='padding:0px 4px;width:16px;'><br>";
					if($states[1][1]!="") echo "<img src='".$states[1][1]."' style='padding:0px 4px;width:16px;'>"; else echo "<div style='display: inline-block;width:16px;'></div>";
					echo '</div>';
					echo "<div style='padding: 7px;display: inline-block;width: 95%;'>".$topName[2];
					echo "<div style='float:right;text-align:right;'><i>- nepřístupné -</i><div class=mini><b>".$news[2]."</b> z ".$news[1]."</div></div>";
					echo "<div class=mini>".$topics[1][3]."</div></div>";
				echo "</div>";
				
			}
		}	
	}else if($type == 2){
		echo "<div style='background-image:url(".Router::url()."/upload/furrici_logo.jpg);height: 26px;background-repeat: no-repeat;background-position: -22px 0px;border-bottom: 1px solid #5D5D5D;background-size: 223px 72px;'></div>";	
		
		$result=str_replace("\n","",board_curl("http://www.furrici.info/forum/"));
		$result=preg_replace('/\s+/', ' ', $result);
		$result=str_replace(' />', '>', $result);
		$usa = preg_match('/\<table class="list forum-topiclist"\>(.*)\<\/table\>/U',  $result, $match);
		
		preg_match_all('/\<tr class="(.*)">(.*)<\/tr>/U', $match[0], $matches, PREG_SET_ORDER);
		$maximum = 5;
		if(count($matches) > $maximum){ $maxim = $maximum; }else{ $maxim = count($matches); }
				
		for($i=0;$i<$maximum;$i++){
			preg_match_all('/\<td class="(.*)"\>(.*)\<\/td\>/U',  $matches[$i][2], $topics, PREG_SET_ORDER);
			$top = preg_match('/\<a href="http:\/\/www.furrici.info\/forum\/tema\/(.*)\/">(.*)<\/a>/U',  $topics[0][2], $topName);
			$states[1][0]="http://www.furrici.info/images/ico.gif"; 
			$usa = preg_match('/\<a>(.*)<\/a><br>(.*)/U',  $topics[1][2], $userTop);		
			$infor = explode("<br>", str_replace("/>","",$topics[2][2]));		
			$odpovedi = explode(" ",trim($infor[0]));
			//$new = preg_match('/(.*) \((.*)\)/U',  $topics[3][3], $news);		
			$user = explode("<br>",$topics[1][2]);
			
			$tmst = explode(" ", trim($user[1]));
			$tms1 = explode(":", $tmst[0]); //12:13
			$tms2 = explode(".", $tmst[1]); //13.12.2011
			$timq = $tms2[2].$tms2[1].$tms2[0].$tms1[0].$tms1[1];
			
			//$data_____["Posts"][] = array("time_stap" => $timq, "state1" => $states[1][0], "state2" => "", "Id" => $topName[1], "Name" => encodeDiacritic(iconv("UTF-8", "Windows-1252//TRANSLIT", $topName[2])), "Section" => trim(encodeDiacritic( iconv("UTF-8", "Windows-1252//TRANSLIT", $infor[1]) )), "Posts" => $odpovedi[0], "Readable" => 1, "User" => encodeDiacritic($userTop[1]), "Time" => trim($user[1]), "New" => 0);
			
			echo "<div class='olozkaid'>";
				echo '<div style="display:inline-block">';
				echo "<img src='".$states[1][0]."' style='padding:0px 4px;'><br>";
				echo "<div style='display: inline-block;width:24px;'></div>";
				echo '</div>';
				echo "<div style='padding: 7px;display: inline-block;width: 95%;'>".$topName[2];
				echo "<div style='float:right;text-align:right;'><b>".$userTop[1]."</b> ".$user[1]."<div class=mini>".$odpovedi[0]."</div></div>";
				echo "<div class=mini>".$infor[1]."</div></div>";
			echo "</div>";
		}		
	}
}

function board_page_draw($t, &$output){	
	echo "<div style='padding:5px;'></div>";
	board_show(1);
	board_show(2);
}