<?php
$this->hook_register("init.setting", "turnaj_init", -10);
$this->hook_register("page.turnaj.init.setting", "turnaj_init_setting", -10);
$this->hook_register("page.turnaj", "article_turnaj_draw", 0);

define("STEAM_TOKEN", "E5E292E1C646D3198C95E83024FB4248");

function resolve_id($id)
{
  if (!preg_match('/^7656119[0-9]{10}$/i', $id))
  {
    $xml = @simplexml_load_file(sprintf("http://api.steampowered.com/ISteamUser/ResolveVanityURL/v0001/?key=%s&format=xml&vanityurl=%s", STEAM_TOKEN, $id));
    return $xml -> success == "1" ? $xml -> steamid : null;
  }
  else
  {
    return $id;
  }
}

function steam_get_profile($id){
	$xml = @simplexml_load_file(sprintf("http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%s&format=xml&steamids=%s", STEAM_TOKEN, $id));
	if(count((array)$xml->players) > 0){
		$data = array(
			"personaname" => $xml->players->player->personaname,
			"profileurl" => $xml->players->player->profileurl,
			"avatar" => $xml->players->player->avatarmedium,
			"steamid" => $xml->players->player->steamid
		);
		return $data;
	}else
		return null;
}

function csgo_last_stat($id, $no = false){
	$xml = @simplexml_load_file(sprintf("http://api.steampowered.com/ISteamUserStats/GetUserStatsForGame/v0002/?appid=730&key=%s&steamid=%s&format=xml", STEAM_TOKEN, $id));
	if(isset($xml->steamID)){
		$stat = array();
		foreach ($xml->stats->stat as $st) {
			if($st->name == "last_match_kills")
				$stat[0] = $st->value;
			if($st->name == "last_match_deaths")
				$stat[1] = $st->value;
			if($st->name == "last_match_t_wins")
				$stat[2] = $st->value;
			if($st->name == "last_match_ct_wins")
				$stat[3] = $st->value;
		}
		return array(
					"last_match_kills" => $stat[0],
					"last_match_deaths" => $stat[1],
					"last_match_t_wins" => $stat[2],
					"last_match_ct_wins" => $stat[3]
				);
	}else if($no == false){
		$d = resolve_id($id);
		$id= steam_get_profile($d);
		return csgo_last_stat($id["steamid"], true);
	}
	return null;
}

function turnaj_init($t){
	$t->router->add("turnaj[/<action=default>][/<id=0>]", "module=turnaj&action=<action>&id=<id>");	
}

function turnaj_init_setting($t){
	$t->root->config->set("style.menu.left", "hide");
	$result = dibi::query("SELECT * FROM :prefix:turnaj WHERE id=%i", $t->router->_data["id"][0])->fetch();
	if($result == NULL){
		$title = "Turnaj neexistuje";
	}else{
		$title = $result["title"];
	}
	$t->root->config->set("pre-title",$title);
}

function getsetting($game){
	$data = array();
	if($game == "CS:GO"){
		$data["back"] = "csgo.jpg";
		$data["ico"] = "csgo.png";
		$data["color"] = "white";
		$data["name"] = "Counter-Strike: Global Offensive";
		$data["url"] = "http://steamcommunity.com/app/730";
	}
	return $data;
}

function article_turnaj_draw($t, &$output){
	if($t->router->_data["action"][0] == "edit"){
		$result = dibi::query("SELECT * FROM :prefix:turnaj WHERE id=%i", $t->router->_data["id"][0])->fetch();
		if($result == NULL){
			$t->root->page->draw_error("Turnaj neexistuje", "Turnaj s ID: ".$t->router->_data["id"][0]." neexistuje!");
		}else{
			$bad = false;
			
			if(isset($_POST["log"])){
				Cookies::set("password_team_".$t->router->_data["id"][0], $_POST["pass"]);
				$_COOKIE["password_team_".$t->router->_data["id"][0]] = $_POST["pass"];
			}
			
			if(!isset($_COOKIE["password_team_".$t->router->_data["id"][0]])){ $bad = true; }
			else if($_COOKIE["password_team_".$t->router->_data["id"][0]] != $result["password"]){ $bad = true; }
			
			$gameset = getsetting($result["game"]);
			
			$output.= "<div style='background-image:url(".Router::url()."/modules/turnaj/background/".$gameset["back"].");background-repeat:no-repeat;padding: 15px 46px;background-size: 100%;'>";
			if($bad){
				$output.= "<h1 style='color: ".$gameset["color"].";'>Zadejte heslo pro editaci akce \"".$result["title"]."\"</h1>";
				$output.= "<form action=# method=post>";
				$output.= "<input type=text name=pass><br><input type=submit name=log value=Odeslat>";
				$output.= "</div>";
			}else{
				$settings = $t->root->config->load($result["settings"]);
				
				if(isset($_POST["upravit"])){
					$settings["type"] = $_POST["type"];
					$settings["teams"] = $_POST["teams"];
					$settings["make"] = $_POST["name_maker"];
					$settings["url"] = $_POST["url_maker"];
					$settings["win"] = $_POST["win"];
					$settings["starttime"] = $_POST["starttime"];
					$settings["chat"] = $_POST["chat"];
					$settings["maps"] = $_POST["maps"];
					
					dibi::query('UPDATE :prefix:turnaj SET ', array("settings" => Config::ssave($settings), "title" => $_POST["nazev"], "game" => $_POST["game"], "password" => $_POST["pass"]), "WHERE `id`=%s", $t->router->_data["id"][0]);
					$t->root->page->error_box("Upraveno", "ok");
					$result = dibi::query("SELECT * FROM :prefix:turnaj WHERE id=%i", $t->router->_data["id"][0])->fetch();
				}
				
				if(isset($_POST["loadstat"])){
					$plas = $t->root->config->load($result["players"]);
					for($i=1;$i<=$settings["teams"];$i++){
						$a = 1;
						while(isset($plas[$i][$a])){
							$id = explode("/", $plas[$i][$a]["steam"]);
							$id = $id[count($id)-2];
							if(isset($plas[$i][$a]["steamid"])) $id = $plas[$i][$a]["steamid"];
							$data = csgo_last_stat($id);
							if($data != null){
								$plas[$i][$a]["last_match_t_wins"] = $data["last_match_t_wins"];
								$plas[$i][$a]["last_match_ct_wins"] = $data["last_match_ct_wins"];
								$plas[$i][$a]["last_match_kills"] = $data["last_match_kills"];
								$plas[$i][$a]["last_match_deaths"] = $data["last_match_deaths"];
							}
							$a++;
						}
					}
					dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
					$t->root->page->error_box("Načteno", "ok");
				}
				
				if(isset($_POST["smazstat"])){
					$plas = $t->root->config->load($result["players"]);
					for($i=1;$i<=$settings["teams"];$i++){
						$a = 1;
						while(isset($plas[$i][$a])){
							$plas[$i][$a]["last_match_t_wins"] = "";
							$plas[$i][$a]["last_match_ct_wins"] = "";
							$plas[$i][$a]["last_match_kills"] = "";
							$plas[$i][$a]["last_match_deaths"] = "";
							$a++;
						}
					}
					dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
					$t->root->page->error_box("Smazano", "ok");
				}
				
				$output.= "<form action=# method=post>";
				$output.= "<div style='font-size:20px;float: right;text-align:right;color: ".$gameset["color"].";background: rgba(0, 0, 0, 0.36);padding: 5px;'><b>Pořádá</b><br><input style='width:250px;' placeholder='Název kdo pořádá akci' type=text name=name_maker value='".$settings["make"]."'><br><input style='width:250px;' placeholder='Odkaz na vaší stránku/skupinu' type=text name=url_maker value='".$settings["url"]."'></div>";
				$output.= "<input type=text name=nazev value='".$result["title"]."' style='width:300px;font-size:20px;'>";
				$output.= "<div style='padding:5px 0px;'>".Utilities::select(array("CS:GO"=>"Counter-Strike: Global Offensive"),$result["game"], "game")."</div>";
				$output.= "<span style='background: #267726;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #304430;font-size: 11px;margin-right:5px;'>".Utilities::select(array("Classic Casual" => "Classic Casual", "Competitive" => "Competitive", "Arms Race" => "Arms Race", "Demolition" => "Demolition", "Deathmatch" => "Deathmatch"), $settings["type"], "type")."</span>";
				$output.= "<span style='background: #0075A2;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #294550;font-size: 11px;margin-right:5px;'>".Utilities::select(array("1" => "1. ".Strings::sklonuj(1,array("tým","týmy","týmů")), "2" => "2. ".Strings::sklonuj(2,array("tým","týmy","týmů")), "3" => "3. ".Strings::sklonuj(3,array("tým","týmy","týmů")), "4" => "4. ".Strings::sklonuj(4,array("tým","týmy","týmů"))), $settings["teams"], "teams")."</span>";
				$output.= "<span style='background: #89A200;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #6A7145;font-size: 11px;margin-right:5px;'>Vítězství ".Utilities::select(array("-1" => "---", "1" => "Tým 1", "2" => "Tým 2", "3" => "Tým 3", "4" => "Tým 4"), $settings["win"], "win")."</span>";
				$output.= "<br><br>";
				$output.= "<font color='".$gameset["color"]."' style='text-shadow: 0px 0px 1px black;'>Změnit heslo: </font><span style='padding-left: 80px;color:".$gameset["color"].";text-shadow: 0px 0px 1px black;'>Čas začátku: </span><span style='padding-left: 80px;color:".$gameset["color"].";text-shadow: 0px 0px 1px black;'>Odkaz na chat: </span><span style='padding-left: 80px;color:".$gameset["color"].";text-shadow: 0px 0px 1px black;'>Mapy: </span><br><input type=text name=pass value='".$result["password"]."'><input type=text name=starttime value='".$settings["starttime"]."'><input type=text name=chat value='".$settings["chat"]."'><input type=text name=maps value='".$settings["maps"]."'><br>";
				$output.= "<input type=submit name=upravit value='Upravit turnaj'> <input type=submit name=loadstat value='Načíst statistiky minuleho zapasu'> <input type=submit name=smazstat value='Smazat statistiky'>";
				$output.= "</form>";
				
				$plas = $t->root->config->load($result["players"]);
				$output.= "<div style='margin-top:30px;'>";
				$output.= "<h1 style='color:".$gameset["color"].";text-shadow: 0px 0px 1px black;'>Uprava učastníků</h1>";
				
					if(isset($_POST["addme"])){
						$team = $_POST["team"];
						$id = resolve_id($_POST["steamurl"]);
						if($id != null){
							$data = steam_get_profile($id);
							if($data != null){
								$a = 1;
								while(isset($plas[$team][$a])){$a++;}
								$plas[$team][$a] = array("steamid" => $data["steamid"], "name" => $data["personaname"], "steam" => $data["profileurl"], "avatar" => $data["avatar"]);
								dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
								$t->root->page->error_box("Byl přidán to teamu ".$team."", "ok");
							}else
								$t->root->page->error_box("Chyba při získávání dat!", "error");
						}else
							$t->root->page->error_box("Steam jméno nebylo nalezeno!", "error");
						$output.="<br>";
					}
					if(isset($_GET["delete"])){
						$smazano = false;
						for($i=1;$i<=$settings["teams"];$i++){
							$a = 1;
							while(isset($plas[$i][$a])){
								if($plas[$i][$a]["name"] == $_GET["delete"] and !$smazano){
									unset($plas[$i][$a]);	
									$smazano = true;				
								}else if($smazano){
									$plas[$i][$a-1] = $plas[$i][$a];
								}
								$a++;
							}
							if($smazano){ unset($plas[$i][$a-1]); }
						}
						dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
						header("location:".$t->router->url_."turnaj/edit/".$t->router->_data["id"][0]);
						break;
					}
				
					if(isset($_POST["edittes"])){
						$plas[$_POST["teamid"]]["name"] = $_POST["teamname"];
						$plas[$_POST["teamid"]]["max"] = $_POST["maxsiz"];
						dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
					}
				
					for($i=1;$i<=$settings["teams"];$i++){
						$output.= "<div style='float:left;margin-right:5px;width: 300px;background: black;margin: 15px 15px;padding: 10px;color:white;'>";
						if(!isset($plas[$i]["name"])) $plas[$i]["name"] = "Tým ".$i;
						$output.= "<b style='margin-bottom: 8px;display: block;'><form action=# method=post style='display:inline-block;'><input type=text name=teamname style='width: 112px;' value='".$plas[$i]["name"]."'> <input type=hidden name=teamid value='".$i."'> | Velikost: <input type=number value='".$plas[$i]["max"]."' name=maxsiz style='width:55px;padding:4px;'><input type=submit name=edittes value='Edit'></form></b>";
						$a = 1;
						while(isset($plas[$i][$a])){
							$pn = $plas[$i][$a];
							$add = " [ <a href='?delete=".$pn["name"]."'>X</a> ]";
							$output.= "<div style='background-repeat: no-repeat;background-size: 30px;background-image:url(".$pn["avatar"].");height: 19px;padding: 7px 38px;color: white;border-bottom: 1px solid #252525;'>".$a.". <a target=_blank href='".$pn["steam"]."'>".$pn["name"]."</a>".$add."</div>";
							$a++;
						}
						for(;$a<=$plas[$i]["max"];$a++){
							$output.= "<div style='height: 19px;padding: 7px 38px;color: white;border-bottom: 1px solid #252525;'>".$a.". <span style='color:silver;font-style:italic;'>#VOLNO#</span></div>";
						}
						$output.="<form action=# method=post style='margin-top:10px;'><input type=hidden name=team value='".$i."'><input type=text name=steamurl style='color:white;width: 200px;background: black;border: 1px solid #6F6F6F;border-right: 0px;' placeholder='Zadejte jméno v url adrese na steamu'><input type=submit name=addme value='Přidat'></form>";
						$output.= "</div>";
					}
					$output.= "<div style='clear:both'></div>";
				$output.= "</div>";
			}
			$output.="</div>";
		}
		$output.= "<a href='".$t->router->url_."turnaj/show/".$t->router->_data["id"][0]."'>Zpátky na turnaj</a>";
	}
	else if($t->router->_data["action"][0] == "show"){
		$result = dibi::query("SELECT * FROM :prefix:turnaj WHERE id=%i", $t->router->_data["id"][0])->fetch();
		if($result == NULL){
			$t->root->page->draw_error("Turnaj neexistuje", "Turnaj s ID: ".$t->router->_data["id"][0]." neexistuje!");
		}else{
			$settings = $t->root->config->load($result["settings"]);
			$gameset = getsetting($result["game"]);
			$output = "";
			$output.= "<div style='background-image:url(".Router::url()."/modules/turnaj/background/".$gameset["back"].");background-repeat:no-repeat;padding: 15px 46px;background-size: 100%;'>";
			$output.= "<div style='font-size:20px;float: right;text-align:right;text-shadow: 0px 0px 1px black;color: ".$gameset["color"].";background: rgba(0, 0, 0, 0.36);padding: 5px;'><b>Pořádá</b><br><a style='color: ".$gameset["color"].";' target=_blank href='".$settings["url"]."'>".$settings["make"]."</a></div>";
			$output.= "<h1 style='margin:0px;padding:0px;text-shadow: 0px 0px 1px black;color:".$gameset["color"].";'>".$result["title"]."</h1>";
			$output.= "<div style='font-weight:bold;background-repeat: no-repeat;background-image:url(".Router::url()."/modules/turnaj/icon/".$gameset["ico"].");background-size: 23px 23px;background-position: 0px 5px;padding: 7px 27px;color: white;text-shadow: 0px 0px 1px black;'>".$gameset["name"]." >> <a style='color:".$gameset["color"].";' href='".$gameset["url"]."' target=_blank>STEAM komunita hry</a></div>";
			$output.= "<span style='background: #267726;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #304430;font-size: 11px;margin-right:5px;'>Typ hry: ".$settings["type"]."</span>";
			$output.= "<span style='background: #0075A2;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #294550;font-size: 11px;margin-right:5px;'>".$settings["teams"]." ".Strings::sklonuj($settings["teams"],array("tým","týmy","týmů"))."</span>";
			$output.= "<span style='background: #89A200;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #6A7145;font-size: 11px;margin-right:5px;'>Začátek ".$settings["starttime"]."</span>";
			if(isset($settings["chat"]))
			$output.= "<span style='background: #A2000F;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #88333B;font-size: 11px;margin-right:5px;'><a target=_blank style='color:".$gameset["color"].";' href='".$settings["chat"]."'>Chat</a></span>";
			if(isset($settings["maps"]))
				if($settings["maps"]!="")
					$output.= "<span style='background: #9800A2;color: white;padding: 4px 10px;border-radius: 3px;display: inline-block;font-weight: bold;border: 1px solid #5A2D5D;font-size: 11px;margin-right:5px;'>".$settings["maps"]."</span>";
			//$output.= "</div>";	
			
			$plas = $t->root->config->load($result["players"]);
			$output.= "<div style='margin-top:5px;'>";
			
				if(isset($_POST["addme"])){
					$team = $_POST["team"];
					$id = resolve_id($_POST["steamurl"]);
					if($id != null){
						$data = steam_get_profile($id);
						if($data != null){
							$a = 1;
							while(isset($plas[$team][$a])){$a++;}
							$plas[$team][$a] = array("name" => $data["personaname"], "steam" => $data["profileurl"], "avatar" => $data["avatar"]);
							dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
							Cookies::set( "inTeam_".$t->router->_data["id"][0], $data["personaname"], "+1 year" );
							$_COOKIE["inTeam_".$t->router->_data["id"][0]] = $data["personaname"];
							$t->root->page->error_box("Byl jsi přidán to teamu ".$team."", "ok");
						}else
							$t->root->page->error_box("Chyba při získávání dat!", "error");
					}else
						$t->root->page->error_box("Steam jméno nebylo nalezeno!", "error");
					$output.="<br>";
				}
				if(isset($_GET["delete"])){
					for($i=1;$i<=$settings["teams"];$i++){
						$a = 1;
						$smazano = false;
						while(isset($plas[$i][$a])){
							if($plas[$i][$a]["name"] == $_COOKIE["inTeam_".$t->router->_data["id"][0]] and !$smazano){
								unset($plas[$i][$a]);	
								$smazano = true;				
							}else if($smazano){
								$plas[$i][$a-1] = $plas[$i][$a];
							}
							$a++;
						}
						if($smazano){ unset($plas[$i][$a-1]); }
					}
					dibi::query('UPDATE :prefix:turnaj SET ', array("players" => Config::ssave($plas)), "WHERE `id`=%s", $t->router->_data["id"][0]);
					Cookies::delete("inTeam_".$t->router->_data["id"][0]);
					header("location:".$t->router->url_."turnaj/show/".$t->router->_data["id"][0]);
					break;
				}
				if(isset($_GET["fuck"])){
					Cookies::delete("inTeam_".$_GET["fuck"]);
				}
			
				for($i=1;$i<=$settings["teams"];$i++){
					$output.= "<div style='float:left;margin-right:5px;width: 300px;background: black;margin: 15px 15px;padding: 10px;color:white;'>";
					if(!isset($plas[$i]["name"])) $plas[$i]["name"] = "Tým ".$i;
					if($settings["win"] == $i)
						$output.= "<b style='margin-bottom: 8px;display: block;background: gold;text-align: center;color: green;'>!!! Vyhrál tým \"".$plas[$i]["name"]."\" !!!</b>";
					else
						$output.= "<b style='margin-bottom: 8px;display: block;'>".$plas[$i]["name"]."</b>";
					$a = 1;
					while(isset($plas[$i][$a])){
						$pn = $plas[$i][$a];
						$add = "";
						$regis = "";
						if(isset($_COOKIE["inTeam_".$t->router->_data["id"][0]])) $regis = $_COOKIE["inTeam_".$t->router->_data["id"][0]];
						if($regis == $pn["name"])
							$add = " [ <a href='?delete'>X</a> ]";
						if(isset($pn["last_match_deaths"])){
							if($pn["last_match_deaths"]!="")
								$output.="<div style='text-align:center;width: 49px;float:right;background: #292929;padding: 8px;'>".$pn["last_match_kills"]." : ".$pn["last_match_deaths"]."</div>";
						}
						$output.= "<div style='background-repeat: no-repeat;background-size: 30px;background-image:url(".$pn["avatar"].");height: 19px;padding: 7px 38px;color: white;border-bottom: 1px solid #252525;'>".$a.". <a target=_blank href='".$pn["steam"]."'>".$pn["name"]."</a>".$add."</div>";
						$a++;
					}
					$volno = false;
					for(;$a<=$plas[$i]["max"];$a++){
						$volno = true;
						$output.= "<div style='height: 19px;padding: 7px 38px;color: white;border-bottom: 1px solid #252525;'>".$a.". <span style='color:silver;font-style:italic;'>#VOLNO#</span></div>";
					}
					if(!$volno)
						$output.= "<span style='color:orange;margin-top:10px;display:block;'>Tento team je plný!</span>";
					elseif(isset($_COOKIE["inTeam_".$t->router->_data["id"][0]]))
						$output.= "<span style='color:red;margin-top:10px;display:block;'>Již jsi v Teamu!</span>";
					else
						$output.="<form action=# method=post style='margin-top:10px;'><input type=hidden name=team value='".$i."'><input type=text name=steamurl style='color:white;width: 240px;background: black;border: 1px solid #6F6F6F;border-right: 0px;' placeholder='Zadejte vaše jméno v url adrese na steamu'><input type=submit name=addme value='Přidat'></form>";
					$output.= "</div>";
				}
				$output.= "<div style='clear:both'></div>";
			$output.= "</div>";
			$output.= "</div>";
		}
		$output.= "<a href='".$t->router->url_."turnaj/edit/".$t->router->_data["id"][0]."'>Upravit turnaj</a> <font color=white>| vytvořil Natsu</font>";
	}else{
		
	}
	?>
	<script>
	setTimeout(function(){ if($(".context").outerHeight(true) < $(window).height()){ $(".context").css("margin-top", (($(window).height()/2)-($(".context").outerHeight(true)/2))+"px"); } } , 100);
	</script>
	<?php
}