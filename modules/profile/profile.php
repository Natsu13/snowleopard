<?php
$this->hook_register("page.profile.init.setting", "profile_init_setting", -10);
$this->hook_register("page.profile", "profile_page_draw", 0);

function profile_init_setting($t){		
	$profil = User::get($t->router->_data["id"][0]);
	if(!$profil){
		$title = "Profil nebyl nalezen";
		$t->root->config->set("pre-title",$title);
	}else{
		$title = $profil["nick"];
		$t->root->config->set("title",$title);
		$t->root->config->set("pre-title","");
	}
}

function profile_page_draw($t, &$output){
	$profil = User::get($t->router->_data["id"][0]);
	
	if($profil == NULL){
		$t->root->page->draw_error("Profil nebyl nalezen", "Profil ".$t->router->_data["id"][0]." neexistuje!");
	}else{
		$data = Config::sload($profil["data"]);
		$output = "<div class='user-background-tile'>";
		
		$output.= "</div>";
		$output.= "<div class='user-background-panel'>";
			$output.= "<span class=tile><span class=namedti>Druh</span> ".$data["druh"]."</span>";
			$output.= "<span class=tile><span class=namedti>Pohlaví</span> ".$data["pohlavi"]."</span>";
		$output.= "</div>";
	}
}