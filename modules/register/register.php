<?php
$this->hook_register("init.setting", "register_init", -10);  //Register the new router
$this->hook_register("page.register.init.setting", "register_init_setting", -10);  //Set title of page
$this->hook_register("page.register", "register_page_draw", 0);  //  Draw a register page

function register_init($t){
	$t->router->add("register", "module=register");	
	$t->root->log("Modul registrace byl úšpěšně načten!");
}

function register_init_setting($t){	
	$t->root->config->set("pre-title","Registrace");
}

function register_page_draw($t, &$output){
	if(isset($_POST["register"])){
		if(!Utilities::captcha_test("kontrolni_kod_registrace", $_POST["captcha"])){
			$t->root->page->error_box("Kontrloní kod byl opsán špatně!", "error");
		}else if($_POST["password"] != $_POST["password2"]){
			$t->root->page->error_box("Zadaná hesla nejsou stejná!", "error");
		}else{
			$create = User::create($_POST["jmeno"], $_POST["password"], $_POST["email"]);
			if(!is_array($create)){
				$t->root->page->error_box("Váš uživatelský účet byl úspěšně vytvořen, můžete se přihlásit.", "ok");
			}else{
				$mess = "Chyba při vytváření účtu: <ul>";
				for($i = 0;$i < count($create); $i++){
					$mess.= "<li>".$create[$i]."</li>";
				}
				$mess.= "</li>";
				$t->root->page->error_box($mess, "error");
			}
		}
	}
	$output = $t->root->page->template_parse(_ROOT_DIR . "/modules/register/register.templatte");
}