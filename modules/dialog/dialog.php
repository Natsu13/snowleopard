<?php
$this->hook_register("init.setting", "dialog_init", -10);
$this->hook_register("page.dialog", "dialog_page_draw", 0);

function dialog_init($t){
	$t->router->add("recovery/password", "module=dialog&id=1&changepass");	
}

function dialog_page_draw($t, &$output){	
	if($_GET["id"] == 1){
		$error = 0;
		$jak = 1;
		if(isset($_GET["user"])){
			if(!User::isExists($_GET["user"])){ $error = 1; }
		}else{ $_GET["user"] = "";$_GET["key"] = ""; }
		
		if($error == 0 and $_GET["user"] != "" and isset($_GET["changepass"]) and $_GET["key"]!=""){
			echo "<div style='width: 390px;border: 1px solid silver;border-bottom: 0px;padding: 15px;background-color: white;'><h2>Vybrat nové heslo</h2>";
			$show = true;
			$user = User::get($_GET["user"]);
			if($user["recovery"] != $_GET["key"]){header("location:/".$t->root->router->_url);}
			if(isset($_POST["heslon"])){
				if($_POST["heslon"] != $_POST["heslo2"]){ $t->root->page->error_box("Zadaná hesla nejsou shodná!","error"); }
				elseif($user["recovery"] != $_GET["key"]){ $t->root->page->error_box("Špatný ověřovací klíč!","error"); }
				elseif(strlen($_POST["heslon"]) < 6 or $_POST["heslon"] == ""){ $t->root->page->error_box("Heslo je přílíš krátké!","error"); }
				else{
					$id = $_GET["user"];
					$arr = array("heslo" => sha1($_POST["heslon"]), "recovery" => "", "passlastchange" => time());
					dibi::query('UPDATE :prefix:users SET ', $arr, 'WHERE `id`=%s', $id, " OR `jmeno`=%s", $id);
					$t->root->page->error_box("Heslo bylo úspěšně změněno, můžete se přihlásit!","green");
					$show = false;
				}
				echo "<br>";
			}
			if($show){				
				echo "<span class='description black'>Změna hesla uživatele <b>".$user["nick"]."</b></span><br>";
				echo "<span class='description black'>Vaše heslo bylo naposledy změněno: <b>".($user["passlastchange"]==""?"Nikdy":Strings::str_time($user["passlastchange"]))."</b></span>";
				echo "<form action=# method=post><table style='margin:20px 0px;margin-left:20px;'>";
				echo "<tr><td>Nové heslo</td><td><input type=password name=heslon id=frm_in_pass1></td><td><button onClick=\"recoveryPass(2);return false;\">?</button></td></tr>";
				echo "<tr><td>Znovu heslo</td><td><input type=password name=heslo2 id=frm_in_pass2></td></tr>";
				echo "</table>";
			}
			echo "</div><div style='width:400px;background-color: #DFDFDF;border: 1px solid silver;height: 26px;padding: 10px;'><div class=dialog-footer-button><input type=submit name=change class='blue button' value=Změnit> <a style='padding: 5px 12px;' class=button href='".$t->root->router->url."'>Zrušit</a></div></div></form>";
		}elseif($error == 0 and $_GET["user"] != ""){			
			if(isset($_GET["key"])){	
				if($_GET["key"] != ""){
					$user = User::get($_GET["user"]);
					if($user["recovery"] != $_GET["key"])
						$error = 1;
					if(trim($_GET["key"]) == "")
						$error = 2;
				}
				
				if($_GET["key"]!="" and $error == 0){
					echo "[Show Change Pass]";
				}else{
					echo "<div class='cnt'>";
						echo "<input type=hidden id='frm_in_username' value='".$_GET["user"]."'>";
						echo "<span class=description style=''>Na vaši emailovou adresu byl odeslán 8 místný klíč zadejte ho prosím níže.</span><br><br>";
						echo "<input type=number style='font-size:25px;background-color:transparent;width:200px;border:0px;".($error!=0?"border-bottom:1px solid red;":"border-bottom:1px solid black;").";' id='frm_in_key' placeholder='XXXXXXXX' value='".$_GET["key"]."'>";
						if($error == 1){ echo "<span class=error style='font-size: 16px;padding: 7px 3px;position: relative;top: -3px;left: -3px;'>Klíč není shodný se zaslaným klíčem!</span>"; }
						elseif($error == 2){ echo "<span class=error style='font-size: 16px;padding: 7px 3px;position: relative;top: -3px;left: -3px;'>Je třeba vyplnit klíč!</span>"; }
					echo "</div>";
				}
			}
			elseif(isset($_GET["recovery"])){
				if($_GET["recovery"]=="2"){
					$rec = User::setRecovery($_GET["user"]);
					if(!$rec) { $error = 1; }
					elseif($rec == -5){ $error = 2; }
				}
				
				if($_GET["recovery"]=="2" and ($error == 0 or $error == 2)){
					echo "[Show key]";
				}else{
					$user = User::get($_GET["user"]);
					echo "<div class='cnt'>";
					echo "<input type=hidden id='frm_in_username' value='".$_GET["user"]."'>";
					echo "<table class=tabfor>";
						echo "<tr><td width=50><img src='".$t->root->router->url."upload/avatars/".$user["avatar"]."' iwdth=48 height=48></td><td><b>".$user["nick"]."</b><br>Uživatelský účet</td></tr>";
						$m=$user["email"];
						$s=explode("@",$m);
						$d="";for($i=0;$i<strlen($s[0])-3;$i++){$d.="*";}
						$m=substr($s[0],0,1).$d.substr($s[0],strlen($s[0])-2,2)."@".$s[1];
						echo "<tr><td colspan=2 class=description style='  margin-top: 9px;display: inline-block;color: #000;'>Na vaší emailovou adresu <b class=description>".$m."</b> bude odeslán potvrzovací kod.</td></tr>";
					echo "</table>";
					if($error == 1){ echo "<span class=error>Při požadavku na obnovu učtu nastala chyba!</span>"; }
					elseif($error == 2){ echo "<span class=error>Tomuto učtu již byl recovery kod nastaven!</span>"; }
					echo "</div>";	
				}				
			}else echo "[Select Recovery]";
		}else{		
			echo "<div class='cnt'>";
			if($jak == 1){
				echo "<table class=tabfor>";
					echo "<tr><td>Zadejte vaše uživatelské jméno nebo email :</td></tr>";
					echo "<tr><td><input type=text name=jmeno id=frm_in_username placeholder='' value='".$_GET["user"]."'></td></tr>";
				echo "</table>";	
				if($error == 1){ echo "<span class=error>Tento uživatelský účet neexistuje!</span>"; }
				echo "<input type=hidden id=hownext value='".$jak."'>";
			}			
			echo "</div>";		
		}
	}
	elseif($_GET["id"] == 2){
		echo "<div class='cnt'>";
		echo "Bezpečené heslo <b>by mělo</b> být delší jak 6 znaků.<br><b>Nemělo by</b> obsahovat slovo snadno nalezitelné v slovníku ani vaše jméno či bydliště<br><b>Mělo by</b> obsahovat minimálně jedno číslo.";
		echo "<br><br><span style=description>Zde si můžete zkopírovat vygenerované heslo<br><input type=text readonly value='".Strings::random(8)."'></span></div>";
	}
	elseif($_GET["id"] == "setHomePage"){
		if(isset($_GET["set"])){
			dibi::query('UPDATE :prefix:users SET ', $arr, 'WHERE `id`=%s', $id, " OR `jmeno`=%s", $id);
		}else{
			$isa = Database::getConfig("default-article");
			echo "<div class='cnt'>";
			echo "Vyberte hlavní článek: <select id='shcl' style='width:300px;'>";
				echo "<option value=-5>Nejnovější články</option>";
				$result = dibi::query('SELECT * FROM :prefix:article');
				foreach ($result as $n => $row) {
					echo "<option value='".$row["id"]."'>".$row["title"]." (".$row["alias"].")</option>";
				}
			echo "</select>";
			echo "</div>";
		}
	}
	elseif($_GET["id"] == "passkontrolhes"){
		$user = User::current();
		if($user["password"] == sha1($_POST["pass"]))
			echo "[SUCCESS]";
		else
			echo "[PASS FAILED]";
	}
}