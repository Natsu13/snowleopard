<?php
$this->hook_register("init.setting", "dagl_init", -10);
$this->hook_register("page.dagl.init.setting", "databaze_init_dagl", -10);
$this->hook_register("page.dagl", "dagl_page_draw", 0);

function dagl_init($t){
	$t->router->add("dagl[/<action=default>]", "module=dagl&action=<action>");	
}

function databaze_init_dagl($t){
	$t->root->config->set("title","Dagl");
	if($t->router->get("action") == "reserve"){
		$t->root->config->set("pre-title","Rezervovat");
	}
	else{		
		$t->root->config->set("pre-title","");
	}
}

function dagl_page_draw($t, &$output){
	$user = User::current();
	$perm = User::permission($user["permission"])["permission"];
	
	if($t->router->get("action") == "reserve"){
		if(isset($_POST["reserver"])){
			$log = Utilities::log("Rezervování nového id s velikostí ".$_POST["ff"],"");
			$data = array("");
			$result = dibi::query('INSERT INTO :prefix:users', $data);
		}
		$output = "<h1>Rezervovat ID</h1>";
		$result = dibi::query('SELECT * FROM dagl_reserve ORDER BY id DESC LIMIT 1')->fetch();
		if($result["id"]==""){$result["id"] = "10";$result["count"]=0;}
		$output.= "Předpokládané přidělené id je <b>".($result["id"]+$result["count"])."</b>";
		$output.= "<form name=reserve action=#>";
			$output.= "<select id=count name=count style='witdh:200px' class=input width=200><option value=0 disabled>- Repository size -</option><option value=100>100</option><option value=200>200</option><option value=300>300</option><option value=400>400</option></select>";
			$output.= "<div class=error_line style='background-image:url(".Router::url()."modules/dagl/images/icon_warning.png);'>Nezapomeňte si vybrat reálnou velikost svého repositáře...<br>V případě zneužívání vám bude udělen ban.</div>";
			$output.= "<input type=submit name=reserver value='Rezervovat'>";
		$output.= "</form>";
	}else{
		$output = "<h1>Systém DAGL</h1>";
		$output.= "+ <a href='".Router::url()."dagl/reserve'>Rezervovat ID</a>";
		
		if($perm["dagl_admin"]==1){
			$output.="<div>Status of reserver id <button>"."Enable"."</button></div>";
		}
		
		$output.="<table class='tablik'>";
		$output.="<tr><th width=350>Rezervované ID</th><th width=350>Délka</th><th width=250>Akce</th></tr>";
		$result = dibi::query('SELECT * FROM dagl_reserve WHERE user=%d', User::current()["id"]);
		foreach ($result as $n => $row) {
			$output.="<tr><td><b>".$row["id"]."</b></td><td>".$row["count"]."</td><td><a href='".$t->router->url_."dagl/repo/".$row["id"]."' class=button>Zvolit tento repositář</a></td></tr>";
		}
		$output.="</table>";
	}
		
	/*
	$result = dibi::query("SELECT * FROM :prefix:article WHERE id=%i", $t->router->_data["id"][0], " or alias=%s", $t->router->_data["id"][0])->fetch();
	if($result == NULL){
		$t->root->page->draw_error("Članek neexistuje", "Článek ".$t->router->_data["id"][0]." neexistuje!");
	}else{
		$output = "<h1>".$result["title"];
		if(User::permission(User::current()["permission"])["permission"]["admin"] == 1){
			$output.="<a class='pen bublina' href=#>Upravit</a><a class='trash bublina' href=#>Smazat</a>";
		}
		$output.= "</h1>";
		$output.= $result["text"];	
	}
	*/
}