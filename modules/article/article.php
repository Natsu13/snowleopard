<?php
$this->hook_register("page.article.init.setting", "databaze_init_setting", -10);
$this->hook_register("page.article", "article_page_draw", 0);

function databaze_init_setting($t){
	if($t->router->_data["id"][0] == NULL){
		$t->router->_data["id"][0] = "index";
		return NULL;
	}
		
	$result = dibi::query("SELECT * FROM :prefix:article WHERE id=%i", $t->router->_data["id"][0], " or alias=%s", $t->router->_data["id"][0])->fetch();
	if($result == NULL){
		$title = "Članek neexistuje";
	}else{
		$title = $result["title"];
	}
	$t->root->config->set("pre-title",$title);
}

function article_page_draw($t, &$output){
	if($t->router->_data["id"][0] == NULL)
		$t->router->_data["id"][0] = "index";
		
	$result = dibi::query("SELECT * FROM :prefix:article WHERE id=%i", $t->router->_data["id"][0], " or alias=%s", $t->router->_data["id"][0])->fetch();
	if($result == NULL){
		$t->root->page->draw_error("Članek neexistuje", "Článek ".$t->router->_data["id"][0]." neexistuje!");
	}else{
		$output="";
		if(User::permission(User::current()["permission"])["permission"]["admin"] == 1){
			$output.="<div class='box tool'><span class=title>Admin panel</span><a href='".$t->router->url."admin/article/edit/".$result["id"]."' class=pen>Upravit</a> | <a href=# class=trash>Smazat</a></div>";
		}
		$output.= "<h1>".$result["title"]."</h1>";
		$output.= $result["text"];	
	}
}