<?php
$this->hook_register("init.setting", "apkinfo_init", -10);
$this->hook_register("page.apkinfo.init.setting", "apkinfo_title", -10);
$this->hook_register("page.apkinfo", "apkinfo_page_draw", 0);

function apkinfo_init($t){
	$t->router->add("apkinfo[/<id=-1>]", "module=apkinfo&id=<id>");	
}

function apkinfo_title($t){
	$t->root->config->set("pre-title","Apk info");
}

function apkinfo_page_draw($t){
	//$t->router->_data["action"][0]
	if(isset($_POST["save"])){
		
	}
?>	
	<script type="text/javascript" src="<?php echo Router::url(); ?>modules/apkinfo/zip.js"></script>
	<script type="text/javascript" src="<?php echo Router::url(); ?>modules/apkinfo/zip-fs.js"></script>
	<script type="text/javascript" src="<?php echo Router::url(); ?>modules/apkinfo/zip-ext.js"></script>
	<script>zip.workerScriptsPath = '<?php echo Router::url(); ?>modules/apkinfo/';</script>
	
	<div style="padding:5px;"></div>
	<h1>APK Info</h1>
	<div id="info">Načítám...</div>	
	<div id="drop_zone" style="border: 2px dashed #bbb;padding: 10px;margin: 15px 0px;color: #bbb;}">Přetánhni soubor APK zde</div>
	<output id="list"></output>
	<form action=# method=post>
		Informace získane ze souboru <i>AndroidManifest.xml</i><br>
		<textarea style="width:100%;padding:3px;resize: none;" rows="5" id="obsah" readonly></textarea>
		<div style="padding:3px;"></div>
		<input type=hidden name=name id=namefile value="">
		<b>Zadejte odkaz na web kde byl odkaz na stažení toho apk souboru: </b><br>
		<input type=text name=url value="http://" style="width:100%;">
		<div style="padding:3px;"></div>
		<b>Zadejte odkaz na web kde byl soubor stažen: </b><br>
		<input type=text name=urldownload value="http://" style="width:100%;">
		<div style="padding:3px;"></div>
		<input type=submit name="save" id=savebutton value="Odeslat" style="padding:8px;" disabled>
	</form>
	<script>
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	  $("#info").html("<div style='background: linear-gradient(to right, green , 1%, transparent);color: green;font-weight: bold;padding: 6px 21px;'>Váš prohlížeč podporuje tuto technologii můžete pokračovat...</div>");
	} else {
	  $("#info").html('<font color=red>The File APIs are not fully supported in this browser.</font>');
	}
	
	var ffff;
	
	(function(obj) {
		var requestFileSystem = obj.webkitRequestFileSystem || obj.mozRequestFileSystem || obj.requestFileSystem;

		function onerror(message) {			
			console.log(message);
		}

		function createTempFile(callback) {
			var tmpFilename = "tmp.dat";
			requestFileSystem(TEMPORARY, 4 * 1024 * 1024 * 1024, function(filesystem) {
				function create() {
					filesystem.root.getFile(tmpFilename, {
						create : true
					}, function(zipFile) {
						callback(zipFile);
					});
				}

				filesystem.root.getFile(tmpFilename, null, function(entry) {
					entry.remove(create, create);
				}, create);
			});
		}

		var model = (function() {
			var URL = obj.webkitURL || obj.mozURL || obj.URL;

			return {
				getEntries : function(file, onend) {
					zip.createReader(new zip.BlobReader(file), function(zipReader) {
						zipReader.getEntries(onend);
					}, onerror);
				},
				getEntryFile : function(entry, creationMethod, onend, onprogress) {
					var writer, zipFileEntry;

					function getData() {
						entry.getData(writer, function(blob) {
							var blobURL = creationMethod == "Blob" ? URL.createObjectURL(blob) : zipFileEntry.toURL();
							onend(blobURL);
						}, onprogress);
					}

					if (creationMethod == "Blob") {
						writer = new zip.BlobWriter();
						getData();
					} else {
						createTempFile(function(fileEntry) {
							zipFileEntry = fileEntry;
							writer = new zip.FileWriter(zipFileEntry);
							getData();
						});
					}
				}
			};
		})();
		
			function handleFileSelect(evt) {
			evt.stopPropagation();
			evt.preventDefault();

			var files = evt.dataTransfer.files; // FileList object.
			ffff  = evt.dataTransfer.files[0];
			// files is a FileList of File objects. List some properties.
			var ihave = false;
			for (var i = 0, f; f = files[i]; i++) {
				var expfile  = f.name.split(".");
				$("#drop_zone").html(f.name);
				$("#drop_zone").css("color", "black");
				if( f.type == "application/x-zip-compressed" || (f.type == "" && expfile[expfile.length - 1] == "apk")){
					$("#namefile").val(f.name);
					model.getEntries(f, function(entries) {
						entries.forEach(function(entry) {
							
							if(entry.filename == "AndroidManifest.xml"){
							
								model.getEntryFile(entry, "File", function(blobURL) {
									$("#obsah").html("Už to bude....");								
									var xhr = new XMLHttpRequest();
									xhr.open('GET', blobURL, true);
									//xhr.responseType = 'blob';
									var lastch = 0, mamt = "";
									xhr.onload = function(e) {
										$("#obsah").html("");
										var mezr = 0;
										for(i=0;i<xhr.response.length;i++){
											var znak = xhr.response[i].charCodeAt(0);
											if( (znak >= 65 && znak <= 90) || ( znak >= 48 && znak <= 57 ) || ( znak >= 97 && znak <= 122 ) || znak == 95 || znak == 46 || znak == 47 || znak == 58 || znak == 45){
												mamt+= xhr.response[i];
												mezr = 0;
											}else{
												if(mezr == 0)
													mamt+= " ";
												else
													mezr++;
											}
											lastch = znak;
										}
										var lastll = " ";
										lastch = " ";
										for(i=0;i<mamt.length;i++){
											var znak = mamt[i];
											if(znak == " " && lastch == " " && lastll != " "){
												$("#obsah").append("\n");
											}else if(znak != " "){
												$("#obsah").append(znak);
											}
											lastll=lastch;
											lastch=znak;
										}
										var data = $("#obsah").html().split("\n");
										$("#obsah").html("");
										for(i=0;i<data.length;i++){
											var row = data[i];
											if(row.length > 1){
												$("#obsah").append(row+"\n");
											}
										}
										
										$("#info").html("<div style='background: linear-gradient(to right, yellow , 1%, transparent);color: black;font-weight: bold;padding: 6px 21px;'>Soubor AndroidManifest.xml byl úspěšně načten můžete pokračovat...</div>");
										$("#savebutton").prop('disabled', false);										
									}
									xhr.send();
									
								}, function(current, total) {
									$("#info").html("<div style='background: linear-gradient(to right, yellow , 1%, transparent);color: black;font-weight: bold;padding: 6px 21px;'>Loading... "+current+"/"+total+"</div>");
								});
								ihave=true;
							}
							
						})
						if(!ihave){
							$("#info").html("<div style='background: linear-gradient(to right, red , 1%, transparent);color: black;padding: 6px 21px;'><b>Toto není platný APK soubor!</b> Chybí AndroidManifest.xml soubor...</div>");
						}
					});
				}else{
					$("#info").html("<div style='background: linear-gradient(to right, red , 1%, transparent);color: black;padding: 6px 21px;'><b>Toto není platný APK soubor!</b> Váš typ souboru je: "+(f.type || "Unkown")+"</div>");
				}
			}		
		  }

		  function handleDragOver(evt) {
			evt.stopPropagation();
			evt.preventDefault();
			evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
		  }

		  // Setup the dnd listeners.
		  var dropZone = document.getElementById('drop_zone');
		  dropZone.addEventListener('dragover', handleDragOver, false);
		  dropZone.addEventListener('drop', handleFileSelect, false);
		  
		})(this);
		  
	var URL = "lorem_store.zip";

	var zipFs = new zip.fs.FS();

	function onerror(message) {
		console.error(message);
	}

	function zipImportedZip(callback) {
		var directory = zipFs.root.addDirectory("import");
		directory.importHttpContent(URL, false, function() {
			zipFs.exportBlob(callback);
		}, onerror);
	}

	function unzipBlob(blob, callback) {
		zipFs.importBlob(blob, function() {
			var directory = zipFs.root.getChildByName("import");
			var firstEntry = directory.children[0];
			firstEntry.getText(callback);
		}, onerror);
	}

	function logText(text) {
		console.log(text);
		console.log("--------------");
	}
	</script>
<?php
}
?>