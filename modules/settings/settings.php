<?php
$this->hook_register("init.setting", "settings_init", -10);
$this->hook_register("page.settings.init.setting", "settings_init_setting", -10);
$this->hook_register("page.settings", "settings_page_draw", 0);

function settings_init($t){
	$t->router->add("settings[/<action=edit>]", "module=settings&action=<action>");	
}

function settings_init_setting($t){
	$t->root->config->set("pre-title","Nastavení");
}

function settings_page_draw($t, &$output){
	if($_GET["action"] == "edit"){
		$user = User::current();
		$output = "<h1>Nastavení účtu</h1>";
		$output.= "<form method=post onSubmit=\"return checkPass('#key', this);\"><input type=hidden name=key id=key value=''><table class=tabfor style='width:600px;'>";
			$output.="<tr><td width=170><label>Přezdívka</label></td><td width=430><input type=text name=nickname value='".$user["nick"]."'></td></tr>";
			$output.="<tr><td></td><td class=description>Jedná se pouze o přezdívku, nikoli o přihlašovací jméno to se změnit nedá.</td></tr>";
			$output.="<tr><td><label>Změna hesla</label></td><td><input type=password name=password value=''></td></tr>";
			$output.="<tr><td></td><td class=description>Vyplňte jen při změně hesla.</td></tr>";
			$output.="<tr><td><label>Email</label></td><td><input type=text name=nickname value='".$user["email"]."'></td></tr>";
			$output.="<tr><td></td><td class=description>Nebude veřejně zveřejněn na vašem profilu.</td></tr>";
			$country = $t->root->config->open("./config/country_list.txt");
			$output.="<tr><td><label>Země</label></td><td><select id=country class='input' style='width:300px;'>";
				$select = 1;
				$i=0;
				foreach($country as $countr){
					if($countr[0] != "English Name")
						$output.="<option value='".$i."' ".($select == $i?"selected":"").">".$countr[0]."</option>";
					$i++;
				}
			$output.="</select></td></tr>";
			
		$output.= "</table>";
		$output.="<div class=dialog-footer-button style='padding-left: 174px;margin-top:18px;float:initial;'><input type=submit class='blue button' name=update value='Upravit profil'></div>";
		$output.="</form>";		
	}else{
		
	}
}