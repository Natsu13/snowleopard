<?php
function match_numbers($numbers){
	$i = 0;
	$count_ = count($numbers) - 1;
	while( $i < $count_ ){
		$a = $i + 1;
		while( $a < $count_ - 1 ){
			if( $numbers[$a] == $numbers[$i] ){
				return true;
			}
			$a++;
		}
		$i++;
	}
	return false;
}

function match_data_second($numbers){
	$i = 0;
	$pole2 = null;
	$count_ = count($numbers);
	while( $i < $count_ ){
		if( isset( $pole2[$numbers[$i]] ) ){ return true; }
		$pole2[$numbers[$i]] = true;
		$i++;
	}
	return false;
}

var_dump(match_numbers(array(1,2,5,1,8,9,1,7)));
var_dump(match_data_second(array(1,2,52,22,8,7,75,7)));
?>