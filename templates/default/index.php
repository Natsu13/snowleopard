<div class="context">
	<div class="page" style="">
		<div class="container">
			<?php if($this->config->get("style.menu.left") != "hide"){ ?>
				<div class="left">
					<ul class="menu">		
						<?php $this->page->menu_draw("left_menu", array("noul" => true, "li_selected_class" => "selected", "a_class" => "href")); ?>
					</ul>
				</div>
			<?php } ?>
			<div class="right" <?php echo ($this->config->get("style.menu.left") == "hide"?"style='width:100%;'":""); ?>>
				<div class="textcont">					
					<?php $this->page->page_draw(); ?>
				</div>
			</div>
			<div style="clear:both;"></div>
		</div>	
	</div>
</div>
<script>
var selectedDIV = "", lastDIVsel = "", selectedBUT = "", selectData = new Array();
var userName = "", key = "";
start();
function recoveryPass(k){
	if(k==1){
		var d = new Dialog();
		d.setTitle('Vyhledejte svůj účet');
		d.setButtons([Dialog.SEARCH, Dialog.CANCEL2]);
		d.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>');
		butt = d.getButtons();
		$(butt[1]).click(function(){ d.Close(); }); //Close
		$(butt[0]).click(function(){ 
			if($("#frm_in_username").val() != ""){
				userName = $("#frm_in_username").val();
				d.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>', { user: $("#frm_in_username").val() }, recoveryPassCheck);
			}else{ $("#frm_in_username").addClass("error"); }
		});
		return false;
	}
	else{
		var d = new Dialog();
		d.setTitle('Bezpečné heslo');
		d.setButtons([Dialog.CLOSE]);
		d.Load('<?php echo $this->router->url."ajax/dialog/2"; ?>');
		butt = d.getButtons();
		$(butt[0]).click(function(){ d.Close(); }); //Close
	}
}
function recoveryPassCheck(dialog, text, status){
	if(text == "[Select Recovery]"){
		var dialog = dialog;
		dialog.setTitle("Obnovit heslo");
		dialog.setButtons([Dialog.CONTINUE, Dialog.CANCEL2]);
		dialog.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>', {user: userName, recovery: 1});
		butt = dialog.getButtons();
		$(butt[1]).click(function(){ dialog.Close(); }); //Close
		$(butt[0]).click(function(){ 
			dialog.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>', { user: $("#frm_in_username").val(), recovery:2 }, recoveryPassCheck);
		});
	}
	if(text == "[Show key]"){
		var dialog = dialog;
		dialog.setTitle("Zkontrolujte doručené e-mailové zprávy");
		dialog.setButtons([Dialog.CONTINUE, Dialog.CANCEL2]);
		dialog.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>', {user: userName, key: ""});
		butt = dialog.getButtons();
		$(butt[1]).click(function(){ dialog.Close(); }); //Close
		$(butt[0]).click(function(){ 
			key = $("#frm_in_key").val();
			dialog.Load('<?php echo $this->router->url."ajax/dialog/1"; ?>', { user: $("#frm_in_username").val(), key:$("#frm_in_key").val() }, recoveryPassCheck);
		});
	}
	if(text == "[Show Change Pass]"){
		window.location.href="<?php echo $this->router->url; ?>recovery/password/?user="+userName+"&key="+key;
	}
}
function passGet(id, form){
	var dialogPass = new Dialog();
	dialogPass.setTitle("Zadejte znovu své heslo");
	dialogPass.setButtons(Dialog.OK_CLOSE);
	dialogPass.dialogHtml.html("<div class=cnt><b>Pro ověření je vyžadováno vaše heslo</b><br><br><label for=pass>Heslo: </label> <input type=password name=pass id=passkontrolhes></div>");
	dialogPass.Show();
	butt = dialogPass.getButtons();
	$(butt[0]).click(function(){ dialogPass.Close(); });
	$(butt[1]).click(function(){ 
		var d = new Dialog();
		d.anonymous = true;
		d.Load('<?php echo $this->router->url."ajax/dialog/passkontrolhes"; ?>', { pass: $("#passkontrolhes").val() }, checkPassowrd);
	});
}

function checkPassowrd(dialog, text, status){
	dialog.Close();
	if(text == "[PASS FAILED]"){
		messageBox("Chyba", "Bylo zadáno špatné heslo!");		
	}else{
		$(idOfPassVal).val(text);
		passGet(idOfPassVal, FormtoPassCheck);
		dialogPass.Close();
	}
}

var idOfPassVal, FormtoPassCheck;
function checkPass(id, form){
	idOfPassVal = id;
	FormtoPassCheck = form;
	if($(id).val() == ""){
		passGet(id, form);
		return false;
	}
	return true;
}
</script>